﻿using CryptoFolio.Core.SQLite;
using CryptoFolio.Core.SQLite.Model;
using System.IO;
using System.Threading.Tasks;
using CryptoFolio.Core.ExchangesHandlers;

namespace CryptoFolio.Console
{
    class Program
    { 
        static void Main(string[] args)
        {
            // Initialize database
            var db = new PortfolioContext();

#if DEBUG
            db.Database.EnsureDeleted();                    
#endif
            db.Dispose();

            // Start cli
            new Cli();
        }
    }
}