﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks.Dataflow;
using CryptoFolio.Core;
using CryptoFolio.Core.Exceptions;
using CryptoFolio.Core.SQLite;
using CryptoFolio.Core.UIClassesModel;
using Microsoft.EntityFrameworkCore;

namespace CryptoFolio.Console
{
    public class CliCommands
    {
        private ManageOrders manageOrders;
        private ManageElements manageElements;
        private ManagePortfolios managePortfolios;
        private ManageUsers manageUsers;
        private ManageCoins coins;

        private BufferBlock<string> loggerDataSource;
        private ConsolePrinter printer;

        public CliCommands(ConsolePrinter printer)
        {
            // Setup logging producer-consumer
            loggerDataSource = new BufferBlock<string>();
            var consumer = new Thread(ConsumeMessages);
            consumer.Start();
            consumer.Join();

            // Initialize database API handler
            manageOrders = new ManageOrders(loggerDataSource);
            manageElements = new ManageElements(loggerDataSource);
            managePortfolios = new ManagePortfolios();
            manageUsers = new ManageUsers();
            coins = new ManageCoins();

            this.printer = printer;
        }

        private async void ConsumeMessages()
        {
            while (true)
            {
                // Read from
                var msg = await loggerDataSource.ReceiveAsync();
                WriteMessage(msg);
            }
        }

        public void WriteMessage(string msg)
        {
            //System.Console.Write(msg);
        }

        public void NewUser()
        {
            // Ask user name and reference coin            
            printer.AddMessage(new ConsoleMessage(1, System.Console.CursorTop,
                "Write user name and reference coin symbol: "));

            var line = System.Console.ReadLine();
            var userParams = line.Split(" ");
            var userName = userParams[0];
            var coin = userParams[1].ToUpper();

            using (var dbs = new PortfolioContext())
            {
                var user = manageUsers.CreateUser(dbs, userName);

                var refCoin = dbs.Coins.Where(c => c.CoinSymbol.Equals(coin)).First();

                managePortfolios.CreatePortfolio(dbs, refCoin, user);
            }
        }

        public void NewOrder()
        {
            // Create order 
            // Ask for quantity, price, date, type, coins            
            printer.AddMessage(new ConsoleMessage(1, System.Console.CursorTop,
                "New Order: ExchangeCoin, BaseCoin, quantity, price, type, " +
                "site, oldFunds [true|false] date[gg/mm/aaaa]: "));

            var line = System.Console.ReadLine();
            var orderConsole = line.Split(" ");

            var quantity = double.Parse(orderConsole[2]);
            var price = double.Parse(orderConsole[3]);
            var type = orderConsole[4];
            var site = orderConsole[5];
            var oldFunds = bool.Parse(orderConsole[6]);
            var date = ParseDate(orderConsole[7]);

            UIOrder order = new UIOrder
            {
                ExchangeCoin = orderConsole[0],
                BaseCoin = orderConsole[1],
                Cost = quantity * price,
                Date = date,
                OldFunds = oldFunds,
                Price = price,
                Quantity = quantity,
                Type = type,
                SiteName = site
            };

            using (var dbs = new PortfolioContext())
            {
                // Add order
                var dbOrder = manageOrders.CreateOrder(dbs, order);

                // Update elements
                manageElements.UpdateElements(dbs, dbOrder);

                // Create trend
                GenerateTrend(dbs, order);

                // TODO: UpdateElementsV2 on AddOrder? Maybe not...
                //api.UpdateElementsV2(db, db.Elements.Single(e => e.Coin == dbOrder.Market.BaseCoin), dbOrder.Market.Site);
                //api.UpdateElementsV2(db, db.Elements.Single(e => e.Coin == dbOrder.Market.ExchangeCoin), dbOrder.Market.Site);

                // Update portfolio
                managePortfolios.UpdatePortfolio(dbs, 1);
            }
        }

        public void DeleteOrder()
        {       
            // Take the order to delete
            using (var dbs = new PortfolioContext())
            {            
                printer.AddMessage(new ConsoleMessage(1, System.Console.CursorTop,
                    manageOrders.PrintAllOrders(dbs)));
                printer.AddMessage(new ConsoleMessage(1, System.Console.CursorTop,
                    "Delete order id: "));
                
                var input = System.Console.Read();
                var orderId = input - 48;

                // Retrieve the order to track and modify
                var order = dbs.Orders.Include(o => o.Market)
                    .Include(m => m.Market.BaseCoin)
                    .Include(m => m.Market.ExchangeCoin)
                    .Include(m => m.Market.Site)
                    .Single(o => o.OrderId == orderId);               

                try
                {
                    manageOrders.DeleteOrder(dbs, order);
                }
                catch (OrderException e)
                {
                    printer.AddMessage(new ConsoleMessage(1, System.Console.CursorTop,
                        e.Message));
                    printer.AddMessage(new ConsoleMessage(1, System.Console.CursorTop,
                        "I can't delete the order :("));                    
                    return;
                }
                managePortfolios.UpdatePortfolio(dbs, 1);
            }
           
        }

        public void ModifyOrder()
        {
            using (var dbs = new PortfolioContext())
            {
                printer.AddMessage(new ConsoleMessage(1, System.Console.CursorTop,
                    manageOrders.PrintAllOrders(dbs)));
                printer.AddMessage(new ConsoleMessage(1, System.Console.CursorTop,
                    "Modify order [id]: "));

                var id = int.Parse(System.Console.ReadLine());

                // Retrieve the order to track and modify
                var oldOrder = dbs.Orders.Include(o => o.Market)
                    .Include(m => m.Market.BaseCoin)
                    .Include(m => m.Market.ExchangeCoin)
                    .Include(m => m.Market.Site)                    
                    .Single(o => o.OrderId == id);

                printer.AddMessage(new ConsoleMessage(1, System.Console.CursorTop,
                    "What you want to modify [Quantity|Price|Both|Date|Type]: "));                
                
                var line = System.Console.ReadLine();

                var toModify = new string[] { };

                switch (line)
                {
                    case "Quantity":
                        printer.AddMessage(new ConsoleMessage(1, System.Console.CursorTop,
                            "Insert new quantity: "));                        
                        var quantity = double.Parse(System.Console.ReadLine());

                        try
                        {
                            manageOrders.ModifyOrder(dbs, oldOrder, manageElements, managePortfolios, "Quantity",
                                new List<object> { quantity });
                        }
                        catch (Exception e)
                        {
                            System.Console.WriteLine(e.Message);
                        }
                        break;

                    case "Price":
                        printer.AddMessage(new ConsoleMessage(1, System.Console.CursorTop,
                            "Insert new price: "));                        
                        var price = double.Parse(System.Console.ReadLine());

                        try
                        {
                            manageOrders.ModifyOrder(dbs, oldOrder, manageElements, managePortfolios, "Price",
                                new List<object> { price });
                        }
                        catch (Exception e)
                        {
                            System.Console.WriteLine(e.Message);
                        }
                        break;

                    case "Date":
                        printer.AddMessage(new ConsoleMessage(1, System.Console.CursorTop,
                            "Insert new date: "));
                        oldOrder.Date = ParseDate(System.Console.ReadLine());
                        dbs.SaveChanges();
                        break;

                    case "Both":
                        printer.AddMessage(new ConsoleMessage(1, System.Console.CursorTop,
                            "Insert new quantity and price: "));
                        var data = System.Console.ReadLine().Split(' ');
                        var q = double.Parse(data[0]);
                        var p = double.Parse(data[1]);

                        try
                        {
                            manageOrders.ModifyOrder(dbs, oldOrder, manageElements, managePortfolios, "Both",
                                new List<object> { q, p });
                        }
                        catch (Exception e)
                        {
                            System.Console.WriteLine(e.Message);
                        }
                        break;
                    case "Type":
                        printer.AddMessage(new ConsoleMessage(1, System.Console.CursorTop,
                            $"Changing type from {oldOrder.Type}"));                        
                        try
                        {
                            manageOrders.ModifyOrder(dbs, oldOrder, manageElements, managePortfolios, "Type",
                                new List<object>());
                        }
                        catch (Exception e)
                        {
                            System.Console.WriteLine(e.Message);
                        }
                        break;
                    case "Oldfunds":
                        printer.AddMessage(new ConsoleMessage(1, System.Console.CursorTop,
                            "You can change oldfunds only if this order is of type Buy"));
                        if (oldOrder.Type.Equals("Buy"))
                        {
                            printer.AddMessage(new ConsoleMessage(1, System.Console.CursorTop,
                                $"Changing oldfunds from {oldOrder.OldFunds}"));                            
                            try
                            {
                                manageOrders.ModifyOrder(dbs, oldOrder, manageElements, managePortfolios, "Oldfunds",
                                    new List<object>());
                            }
                            catch (Exception e)
                            {
                                System.Console.WriteLine(e.Message);
                            }
                        }
                        break;
                }
                managePortfolios.UpdatePortfolio(dbs, 1);
            }            
        }

        public void GenerateTrend(PortfolioContext dbs, UIOrder order)
        {
            var market = dbs.Markets.Single(m => m.BaseCoinSymbol.Equals(order.BaseCoin) &&
                                                m.ExchangeCoinSymbol.Equals(order.ExchangeCoin) &&
                                                m.SiteName.Equals(order.SiteName));

            var exchEl = dbs.Elements.Single(e => e.Coin == market.ExchangeCoin);
            var baseEl = dbs.Elements.Single(e => e.Coin == market.BaseCoin);

            printer.AddMessage(new ConsoleMessage(1, System.Console.CursorTop,
                "Candle dimension [1h - 2h - 4h - 1d - 2d - 4d - 1w]: "));            
            var input = System.Console.ReadLine();
            input = input?.ToUpper();

            var trendFactory = ChooseFactory(input);
            
            manageElements.SetHistoricalData(dbs, baseEl, CandleFactory.CreateCandle(input));
            manageElements.SetHistoricalData(dbs, exchEl, CandleFactory.CreateCandle(input));

            manageElements.GenerateTrend(dbs, baseEl, trendFactory);
            manageElements.GenerateTrend(dbs, exchEl, trendFactory);
        }

        private TrendFactory ChooseFactory(string unit)
        {
            unit = unit.Remove(0, 1);

            switch (unit)
            {
                case "H":
                    return new TrendFactoryHour(manageElements);
                case "D":
                    return new TrendFactoryDay(manageElements);
                default:
                    throw new Exception("Didn't specify candle dimension.");
            }
        }

        private DateTime ParseDate(string stringDate)
        {
            var singles = stringDate.Split("/");
            var gg = int.Parse(singles[0]);
            var mm = int.Parse(singles[1]);
            var aaaa = int.Parse(singles[2]);
            return new DateTime(aaaa, mm, gg);
        }
    }
}
