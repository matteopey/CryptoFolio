﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CryptoFolio.Console
{
    public class ConsoleMessage
    {
        public int X { get; }
        public int Y { get; }
        public string Message { get; }  
        public Maybe<int> DeleteWidth { get; set; }

        public ConsoleMessage(int x, int y, string msg, Maybe<int> deleteWidth = null)
        {
            this.X = x;
            this.Y = y;
            this.Message = msg;

            if (deleteWidth != null)
            {
                this.DeleteWidth = deleteWidth;
            }
            else
            {
                this.DeleteWidth = new Maybe<int>();
            }
        }
    }
}
