﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CryptoFolio.Console
{
    public class ConsolePrinter
    {
        private BlockingCollection<ConsoleMessage> messages;
        private bool stopped = false;

        public ConsolePrinter()
        {
            messages = new BlockingCollection<ConsoleMessage>();
        }

        public void AddMessage(ConsoleMessage msg)
        {
            messages.Add(msg);
        }

        public void Print()
        {
            do
            {
                var msg = messages.Take();
                var x = msg.X;
                var y = msg.Y;

                if (msg.DeleteWidth.Any())
                {
                    System.Console.SetCursorPosition(x, y);
                    System.Console.Write(new string(' ', msg.DeleteWidth.Single()));
                }

                System.Console.SetCursorPosition(x, y);
                System.Console.Write(msg.Message);
            }
            while (stopped == false);

            // Reset cursor position
            System.Console.SetCursorPosition(0, 0);
        }

        public void Stop()
        {
            stopped = true;
        }
    }
}
