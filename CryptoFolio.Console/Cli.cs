﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Threading.Tasks.Dataflow;
using CryptoFolio.Core;
using CryptoFolio.Core.ExchangesHandlers;
using CryptoFolio.Core.SQLite;
using CryptoFolio.Core.SQLite.Model;
using Microsoft.EntityFrameworkCore;

namespace CryptoFolio.Console
{
    // TODO: implement clean exit with signaling. Now all threads continues indefinitely.
    public class Cli
    {
        // Old window settings
        private int saveBufferWidth;
        private int saveBufferHeight;
        private int saveWindowHeight;
        private int saveWindowWidth;

        // New window settings
        private int windowHeight = 52;
        private int windowWidth = 150;

        // Static dimensions for the element
        private readonly int elementWidth = 40;
        private int elementHeight;

        private ManageElements manageElements;
        private ManagePortfolios managePortfolios = new ManagePortfolios();
        private CliCommands commands;

        private EventWaitHandle _waitDownload = new AutoResetEvent(false);
        private EventWaitHandle _commandPressed = new ManualResetEvent(true);
        private bool stop = false;

        private Downloader downloader;
        private ConsolePrinter printer;

        private BufferBlock<string> loggerDataSource;        

        public Cli()
        {
            printer = new ConsolePrinter();

            commands = new CliCommands(printer);
            loggerDataSource = new BufferBlock<string>();
            manageElements = new ManageElements(loggerDataSource);            

            // Save current view
            saveBufferWidth = System.Console.BufferWidth;
            saveBufferHeight = System.Console.BufferHeight;
            saveWindowHeight = System.Console.WindowHeight;
            saveWindowWidth = System.Console.WindowWidth;

            System.Console.SetWindowSize(windowWidth, windowHeight);

            // Start console messages printing
            var consoleMessagesT = new Thread(printer.Print);
            consoleMessagesT.Start();

            // Start download thread
            downloader = new Downloader(_commandPressed, manageElements, _waitDownload);

            // Check if database exists
            CheckDatabase();

            var downloadT = new Thread(downloader.Download);
            downloadT.Start();

            System.Console.Clear();           

            // Start command line thread
            var commandT = new Thread(CommandLine);
            commandT.Start();

            // Start presentation thread
            var presentationT = new Thread(RefreshData);
            presentationT.Start();            

            printer.AddMessage(new ConsoleMessage(1, 1, "User: "));
            printer.AddMessage(new ConsoleMessage(1, 2, "Reference coin: "));
            printer.AddMessage(new ConsoleMessage(1, 5, "Elements: "));            

            // Hide cursor
            System.Console.CursorVisible = false;

            consoleMessagesT.Join();
            commandT.Join();
            presentationT.Join();
            downloadT.Join();

            // Reset console
            System.Console.CursorVisible = true;
            System.Console.SetWindowSize(saveWindowWidth, saveWindowHeight);
            System.Console.SetBufferSize(saveBufferWidth, saveBufferHeight);
            System.Console.Clear();

            System.Console.WriteLine("Cryptofolio end");
        }

        private void CheckDatabase()
        {
            // Initialize database
            var db = new PortfolioContext();

            // If database doesn't exists, this call return true
            // then we load it with all exchanges
            if (db.Database.EnsureCreated())
            {
                downloader.InitialLoad().Wait();
                commands.NewUser();
            }
            db.Dispose();
        }        

        private void CommandLine()
        {
            do
            {                
                var msg = "Commands: STOP, NewOrder-o, DeleteOrder-d, ModifyOrder-m, NewUser-u, " +
                    "PrintAll-pa, PrintCoins-pc, DeleteElement-de, UpdateData-up, ChangeSite-cs";
                printer.AddMessage(new ConsoleMessage(1, windowHeight - 5, msg));
                printer.AddMessage(new ConsoleMessage(1, windowHeight - 4, "To insert a command press 'c'"));

                var trigger = System.Console.ReadKey();
                if (trigger.KeyChar == 'c')
                {
                    // Set cursor visible, so user knows where he is writing
                    System.Console.CursorVisible = true;

                    // Block all threads
                    _commandPressed.Reset();

                    printer.AddMessage(new ConsoleMessage(1, windowHeight - 5 + 1, "Command: ",
                        new Maybe<int>(System.Console.BufferWidth)));

                    var command = System.Console.ReadLine();
                    switch (command.ToLower())
                    {
                        case "o":
                            commands.NewOrder();
                            break;
                        case "d":
                            commands.DeleteOrder();
                            break;
                        case "m":
                            commands.ModifyOrder();
                            break;
                        case "u":
                            System.Console.Write("New user");
                            break;
                        case "up":
                            System.Console.Write("Update data");
                            break;
                        case "cs":
                            System.Console.Write("Change active site");
                            break;
                        case "help":
                            System.Console.Write(
                                "Commands: STOP, NewOrder-o, DeleteOrder-d, ModifyOrder-m, NewUser-u, PrintCoins-pc, DeleteElement-de, ChangeSite-cs : ");
                            break;
                        case "stop":
                            System.Console.WriteLine("Stopping application...");
                            downloader.Stop();
                            printer.Stop();
                            stop = true;
                            _commandPressed.Set();
                            return;
                        default: continue;
                    }
                }
                System.Console.CursorVisible = false;

                // Delete last 10 lines              
                for (int i = 2; i < 12; i++)
                {
                    printer.AddMessage(new ConsoleMessage(1, windowHeight - 5 + i, "",
                        new Maybe<int>(System.Console.BufferWidth)));
                }          

                _commandPressed.Set();

            } while (true);
        }

        private void RefreshData()
        {            
            do
            {
                // Continue only if the signal is free
                _commandPressed.WaitOne();

                // Wait signal from downloadT
                _waitDownload.WaitOne();                

                // Print updated data
                using (var dbs = new PortfolioContext())
                {
                    PrintElements(dbs);
                    PrintUserData(dbs);
                    PrintPortfolio(dbs);
                }

                // Reset waiter. The wait will happen at the beginning
                _waitDownload.Reset();

            } while (stop == false);
            System.Console.WriteLine("Refresh data thread end");
        }

        private void PrintElements(PortfolioContext db)
        {
            // Initial coordinates for rectangle
            var x = 2;
            var y = 5;

            var num = 0;
            var prevHeight = 0;
            elementHeight = 0;

            // Load
            var elements = db.Elements
                .Include(el => el.Costs)
                .ToList();

            foreach (var e in elements)
            {
                y += prevHeight + 1;

                // Print rectangle
                // Height is number of costs + 1 and always >= 4
                var height = 4;

                if (e.Costs.Count > 2)
                {
                    height += e.Costs.Count - 2;
                }
                PrintRect(x, y, elementWidth, height);

                // Print data
                //var top = y + elementHeight * num;
                var top = y + 1;
                printer.AddMessage(new ConsoleMessage(x + 1, top, e.CoinSymbol, new Maybe<int>(5)));
                printer.AddMessage(new ConsoleMessage(x + 1, top + 1, "Quantity: " + e.TotalQuantity,
                    new Maybe<int>(elementWidth - 1)));
                printer.AddMessage(new ConsoleMessage(x + 1, top + 2, "Value: " + e.TotalValue,
                    new Maybe<int>(elementWidth - 1)));
                printer.AddMessage(new ConsoleMessage(x + 20, top, "Costs:"));

                var i = 1;
                foreach (var c in e.Costs)
                {
                    printer.AddMessage(new ConsoleMessage(x + 20 + 1, top + i, 
                        c.BaseCoinSymbol + " = " + c.Cost));                    
                    i++;
                }

                prevHeight = height;
                elementHeight += height + 1;
                num++;
            }
        }

        private void PrintUserData(PortfolioContext db)
        {
            var userName = db.Users.Single().UserName;
            var refCoin = db.Portfolios.Single().CoinSymbol;

            printer.AddMessage(new ConsoleMessage(1, 1, $"User: {userName}"));
            printer.AddMessage(new ConsoleMessage(1, 2, $"Reference coin: {refCoin}"));            
        }

        private void PrintPortfolio(PortfolioContext db)
        {
            var p = db.Portfolios.Single();
            printer.AddMessage(new ConsoleMessage(1, 3, $"Portfolio value: {p.TotalValue}",
                new Maybe<int>(elementWidth)));   
        }

        private void PrintRect(int x, int y, int width, int height)
        {
            // Print width lines            
            for (var i = 0; i < width; i++)
                printer.AddMessage(new ConsoleMessage(x + i, y, "\u2500"));                
                        
            for (var i = 0; i < width; i++)
                printer.AddMessage(new ConsoleMessage(x + i, y + height, "\u2500"));

            // Print corners
            printer.AddMessage(new ConsoleMessage(x, y, "\u250c"));
            printer.AddMessage(new ConsoleMessage(x + width, y, "\u2510"));
            printer.AddMessage(new ConsoleMessage(x, y + height, "\u2514"));
            printer.AddMessage(new ConsoleMessage(x + width, y + height, "\u2518"));
                        
            // Print height lines
            for (var i = 1; i < height; i++)
                printer.AddMessage(new ConsoleMessage(x, y + i, "\u2502\n"));                
            
            for (var i = 1; i < height; i++)
                printer.AddMessage(new ConsoleMessage(x + width, y + i, "\u2502\n"));            
        }


    }
}