# CryptoFolio.Console

Code that open an interactive console for using the program.

### Build
```shell
dotnet restore
dotnet build -c Debug
```

### Run
```shell
dotnet run
```

### Publish
```shell
dotnet publish -c Release -r win10-x64
```