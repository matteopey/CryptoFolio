﻿using System;
using System.Linq;
using Xunit;
using CryptoFolio.Core.Exceptions;

namespace CryptoFolio.Tests.LiquiDataTest
{
    public class InsertOrderTests
    {
        private TestBaseLiqui testBase;
        private DatabaseApiCallsCaller handler;

        // Set-up database and load sample data
        public InsertOrderTests()
        {
            testBase = new TestBaseLiqui("Offline");
            testBase.DbSetUpVoid();

            // Set up
            testBase.LoadCoins();
            testBase.LoadMarkets();

            handler = new DatabaseApiCallsCaller(testBase);
        }
        
        [Fact]
        public void InsertOrder_Scenario1_ShouldUpdateEverythingCorrectly()
        {
            var orders =
                testBase.LoadOrdersFile(
                    @"..\..\..\LiquiDataTest\Files\scenario_1.json");

            // Insert orders
            handler.InsertSampleOrders(orders);

            // Test
            var p = testBase.db.Portfolios.Single();
            var eth = testBase.db.Elements.Single(e => e.Coin.CoinSymbol.Equals("ETH"));
            var btc = testBase.db.Elements.Single(e => e.Coin.CoinSymbol.Equals("BTC"));

            Assert.Equal(8.55719673D, Math.Round(p.TotalValue, 8));

            Assert.Equal(0, eth.TotalQuantity);
            Assert.Equal(0, eth.Costs[0].Cost);
            Assert.Equal(0, eth.TotalValue);

            Assert.Equal(0.5, btc.TotalQuantity);
            Assert.Equal(8.55719673D, Math.Round(btc.TotalValue, 8));
            Assert.Equal(0, btc.Costs.Count);
        }
        
        [Fact]
        public void InsertOrder_Scenario2_ShouldThrowElementException()
        {
            var orders =
                testBase.LoadOrdersFile(
                    @"..\..\..\LiquiDataTest\Files\scenario_2.json");

            // Test
            Assert.Throws<ElementException>(() => handler.InsertSampleOrders(orders));
        }
        
        [Fact]
        public void InsertOrder_Scenario3_ShouldThrowElementException()
        {
            var orders =
                testBase.LoadOrdersFile(
                    @"..\..\..\LiquiDataTest\Files\scenario_3.json");

            // Test
            Assert.Throws<ElementException>(() => handler.InsertSampleOrders(orders));
        }

        [Fact]
        public void InsertOrder_Scenario4_ShouldUpdateEverythingCorrectly()
        {
            var orders =
                testBase.LoadOrdersFile(
                    @"..\..\..\LiquiDataTest\Files\scenario_4.json");

            // Insert orders
            handler.InsertSampleOrders(orders);

            // Test
            var p = testBase.db.Portfolios.Single();
            var eth = testBase.db.Elements.Single(e => e.Coin.CoinSymbol.Equals("ETH"));
            var btc = testBase.db.Elements.Single(e => e.Coin.CoinSymbol.Equals("BTC"));

            Assert.Equal(59.90037712D, Math.Round(p.TotalValue, 8));

            Assert.Equal(0, eth.TotalQuantity);
            Assert.Equal(0, eth.Costs[0].Cost);
            Assert.Equal(0, eth.TotalValue);

            Assert.Equal(3.5, btc.TotalQuantity);
            Assert.Equal(18000, btc.Costs[0].Cost);
            Assert.Equal(59.90037712D, Math.Round(btc.TotalValue, 8));
        }

        [Fact]
        public void InsertOrder_Scenario5_ShouldUpdateEverythingCorrectly()
        {
            var orders =
                testBase.LoadOrdersFile(
                    @"..\..\..\LiquiDataTest\Files\scenario_5.json");

            // Insert orders
            handler.InsertSampleOrders(orders);

            // Test
            var p = testBase.db.Portfolios.Single();
            var eth = testBase.db.Elements.Single(e => e.Coin.CoinSymbol.Equals("ETH"));
            var btc = testBase.db.Elements.Single(e => e.Coin.CoinSymbol.Equals("BTC"));

            Assert.Equal(62.78598365D, Math.Round(p.TotalValue, 8));

            Assert.Equal(20, eth.TotalQuantity);
            Assert.Equal(1, eth.Costs[0].Cost);
            Assert.Equal(20, eth.TotalValue);

            Assert.Equal(2.5, btc.TotalQuantity);
            Assert.Equal(15000, btc.Costs[0].Cost);
            Assert.Equal(42.78598365D, Math.Round(btc.TotalValue, 8));
        }

        [Fact]
        public void InsertOrder_Scenario6_ShouldThrowElementException()
        {
            var orders =
                testBase.LoadOrdersFile(
                    @"..\..\..\LiquiDataTest\Files\scenario_6.json");

            // Insert orders
            Assert.Throws<ElementException>(() => handler.InsertSampleOrders(orders));
        }

        [Fact]
        public void InsertOrder_Scenario7_ShouldUpdateEverythingCorrectly()
        {
            var orders =
                testBase.LoadOrdersFile(
                    @"..\..\..\LiquiDataTest\Files\scenario_7.json");

            // Insert orders
            handler.InsertSampleOrders(orders);

            // Test
            var p = testBase.db.Portfolios.Single();
            var eth = testBase.db.Elements.Single(e => e.Coin.CoinSymbol.Equals("ETH"));
            var dnt = testBase.db.Elements.Single(e => e.Coin.CoinSymbol.Equals("DNT"));

            Assert.Equal(10.1832D, Math.Round(p.TotalValue, 8));

            Assert.Equal(9, eth.TotalQuantity);
            Assert.Equal(0.45, eth.Costs[0].Cost);
            Assert.Equal(9, eth.TotalValue);

            Assert.Equal(10000, dnt.TotalQuantity);
            Assert.Equal(1, dnt.Costs[0].Cost);
            Assert.Equal(1.1832D, Math.Round(dnt.TotalValue, 8));
        }

        [Fact]
        public void InsertOrder_Scenario8_ShouldUpdateEverythingCorrectly()
        {
            var orders =
                testBase.LoadOrdersFile(
                    @"..\..\..\LiquiDataTest\Files\scenario_8.json");

            // Insert orders
            handler.InsertSampleOrders(orders);

            // Test
            var p = testBase.db.Portfolios.Single();
            var eth = testBase.db.Elements.Single(e => e.Coin.CoinSymbol.Equals("ETH"));
            var btc = testBase.db.Elements.Single(e => e.Coin.CoinSymbol.Equals("BTC"));
            var dnt = testBase.db.Elements.Single(e => e.Coin.CoinSymbol.Equals("DNT"));

            Assert.Equal(10.11556787D, Math.Round(p.TotalValue, 8));

            Assert.Equal(9.3, eth.TotalQuantity);
            Assert.Equal(2700, eth.Costs[0].Cost);
            Assert.Equal(9.3, eth.TotalValue);

            Assert.Equal(0.02, btc.TotalQuantity);
            Assert.Equal(0, btc.Costs.Count);
            Assert.Equal(0.34228787D, Math.Round(btc.TotalValue, 8));

            Assert.Equal(4000, dnt.TotalQuantity);
            Assert.Equal(0.4, dnt.Costs[0].Cost);
            Assert.Equal(0.47328D, Math.Round(dnt.TotalValue, 8));
        }

        [Fact]
        public void InsertOrder_Scenario9_ShouldThrowElementException()
        {
            var orders =
                testBase.LoadOrdersFile(
                    @"..\..\..\LiquiDataTest\Files\scenario_9.json");

            // Insert orders
            Assert.Throws<ElementException>(() => handler.InsertSampleOrders(orders));
        }

        [Fact]
        public void InsertOrder_Scenario10_ShouldUpdateEverythingCorrectly()
        {
            var orders =
                testBase.LoadOrdersFile(
                    @"..\..\..\LiquiDataTest\Files\scenario_10.json");

            // Insert orders
            handler.InsertSampleOrders(orders);

            // Test
            var p = testBase.db.Portfolios.Single();
            var steem = testBase.db.Elements.Single(e => e.Coin.CoinSymbol.Equals("STEEM"));
            var btc = testBase.db.Elements.Single(e => e.Coin.CoinSymbol.Equals("BTC"));

            Assert.Equal(87.28511809D, Math.Round(p.TotalValue, 8));

            Assert.Equal(10000, steem.TotalQuantity);
            Assert.Equal(1.1, steem.Costs[0].Cost);
            Assert.Equal(20.53898359D, Math.Round(steem.TotalValue, 8));

            Assert.Equal(3.9, btc.TotalQuantity);
            Assert.Equal(23400, btc.Costs[0].Cost);
            Assert.Equal(66.7461345D, Math.Round(btc.TotalValue, 8));
        }

        [Fact]
        public void InsertOrder_Scenario12_ShouldUpdateEverythingCorrectly()
        {
            var orders =
                testBase.LoadOrdersFile(
                    @"..\..\..\LiquiDataTest\Files\scenario_12.json");

            // Insert orders
            handler.InsertSampleOrders(orders);

            // Test
            var p = testBase.db.Portfolios.Single();
            var eth = testBase.db.Elements.Single(e => e.Coin.CoinSymbol.Equals("ETH"));
            var btc = testBase.db.Elements.Single(e => e.Coin.CoinSymbol.Equals("BTC"));

            Assert.Equal(71.34318038D, Math.Round(p.TotalValue, 8));

            Assert.Equal(20, eth.TotalQuantity);
            Assert.Equal(20, eth.TotalValue);
            var usdtCost = eth.Costs.Single(c => c.BaseCoinSymbol.Equals("USDT"));
            var btcCost = eth.Costs.Single(c => c.BaseCoinSymbol.Equals("BTC"));
            Assert.Equal(3000, usdtCost.Cost);
            Assert.Equal(0.5D, btcCost.Cost);

            Assert.Equal(3, btc.TotalQuantity);
            Assert.Equal(18000, btc.Costs[0].Cost);
            Assert.Equal(51.34318038D, Math.Round(btc.TotalValue, 8));
        }

        [Fact]
        public void InsertOrder_Scenario13()
        {
            var orders =
                testBase.LoadOrdersFile(
                    @"..\..\..\LiquiDataTest\Files\scenario_13.json");

             // Insert orders
            handler.InsertSampleOrders(orders);

            // Test
            var p = testBase.db.Portfolios.Single();
            var eth = testBase.db.Elements.Single(e => e.Coin.CoinSymbol.Equals("ETH"));
            var btc = testBase.db.Elements.Single(e => e.Coin.CoinSymbol.Equals("BTC"));
            var usdt = testBase.db.Elements.Single(e => e.Coin.CoinSymbol.Equals("USDT"));

            Assert.Equal(21.69151477D, Math.Round(p.TotalValue, 8));

            Assert.Equal(8, eth.TotalQuantity);
            Assert.Equal(1600, eth.Costs[0].Cost);
            Assert.Equal(8, eth.TotalValue);

            Assert.Equal(0.8, btc.TotalQuantity);
            Assert.Equal(4600, btc.Costs[0].Cost);
            Assert.Equal(13.69151477D, Math.Round(btc.TotalValue, 8));

            Assert.Equal(0, usdt.TotalQuantity);            
            Assert.Equal(0, usdt.TotalValue);
        }
    }
}