﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks.Dataflow;
using CryptoFolio.Core.SQLite;
using CryptoFolio.Core.SQLite.Model;
using Xunit;
using CryptoFolio.Core.UIClassesModel;
using ExchangesApi;
using ExchangesApi.Exchanges.LiquiApi;
using ExchangesApi.Exchanges.LiquiApi.Data;

[assembly: CollectionBehavior(DisableTestParallelization = true)]

namespace CryptoFolio.Tests.LiquiDataTest
{
    public class TestBaseLiqui
    {
        public PortfolioContext db;
        public MarketsInfo marketsInfo;
        public MarketsTicker marketsTicker;
        private Loader loader = new Loader();
        private Liqui liqui;
        public BufferBlock<string> log;

        public TestBaseLiqui(string type)
        {
            db = new PortfolioContext();
            liqui = new Liqui(new Maybe<IDownloadData>());
            log = new BufferBlock<string>();

            switch (type)
            {
                case "Offline":
                    OfflineData();
                    break;
                case "Online":
                    OnlineData();
                    break;
                default: throw new Exception();
            }
        }

        public void DbSetUpVoid()
        {
            // Reset db
            db.Database.EnsureDeleted();
            db.Database.EnsureCreated();
        }

        private string OpenFile(string path)
        {
            // Open local file
            FileStream fileStream = File.OpenRead(path);

            // Read data
            string data = string.Empty;
            if (fileStream.CanRead)
            {
                var stream = new StreamReader(fileStream);
                data = stream.ReadToEnd();
            }
            return data;
        }

        public void OfflineData()
        {
            const string marketsInfoPath =
                @"..\..\..\LiquiDataTest\Files\marketsInfo.json";
            marketsInfo = JsonConvert.DeserializeObject<MarketsInfo>(OpenFile(marketsInfoPath));

            const string marketsTickerPath =
                @"..\..\..\LiquiDataTest\Files\marketsTicker.json";
            marketsTicker = JsonConvert.DeserializeObject<MarketsTicker>(OpenFile(marketsTickerPath));
        }

        private void OnlineData()
        {
            marketsInfo = liqui.MarketsInfo().Result;
        }

        public void LoadCoins()
        {
            loader.LoadCoins(this);

            // Add site
            db.Sites.Add(new Site()
            {
                SiteName = "Liqui",
                //Coins = db.Coins.ToList(),
                Url = @"https://liqui.io"
            });
            db.SaveChanges();

            // Add user
            db.Users.Add(new User
            {
                UserName = "Test",
                //ReferenceCoin = db.Coins.Single(c => c.Symbol.Equals("ETH"))
            });
            db.SaveChanges();

            // Add portfolio
            db.Portfolios.Add(new Portfolio
            {
                TotalValue = 0,
                User = db.Users.Single(u => u.UserName.Equals("Test")),
                Coin = db.Coins.Single(c => c.CoinSymbol.Equals("ETH"))
            });
            db.SaveChanges();
        }

        public void LoadMarkets()
        {
            loader.LoadMarkets(this);
        }

        public List<UIOrder> LoadOrdersFile(string path)
        {
            // Open local file
            var fileStream = File.OpenRead(path);

            // Read data
            var data = string.Empty;
            if (fileStream.CanRead)
            {
                var stream = new StreamReader(fileStream);
                data = stream.ReadToEnd();
            }

            return JsonConvert.DeserializeObject<List<UIOrder>>(data);
        }
    }
}