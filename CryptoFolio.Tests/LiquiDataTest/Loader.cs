﻿using System.Collections.Generic;
using CryptoFolio.Core.SQLite.Model;

namespace CryptoFolio.Tests.LiquiDataTest
{
    class Loader
    {
        public void LoadCoins(TestBaseLiqui testBase)
        {
            var coinsList = new List<string>();

            foreach (var c in testBase.marketsInfo.Pairs)
            {
                string[] coins = c.Key.Split('_');

                // ToUpper for universal policy
                var exc = coins[0].ToUpper();
                var bas = coins[1].ToUpper();

                if (!coinsList.Contains(exc))
                {
                    coinsList.Add(exc);
                }

                if (!coinsList.Contains(bas))
                {
                    coinsList.Add(bas);
                }
            }

            foreach (var c in coinsList)
            {
                testBase.db.Coins.Add(new Coin()
                {
                    CoinSymbol = c,
                    CoinName = ""
                });
            }
            testBase.db.SaveChanges();
        }

        public void LoadMarkets(TestBaseLiqui testBase)
        {
            foreach (var m in testBase.marketsTicker)
            {
                var coins = m.Key.Split('_');
                var exch = coins[0].ToUpper();
                var bas = coins[1].ToUpper();
                testBase.db.Markets.Add(new Market()
                {
                    Name = m.Key.ToUpper(),
                    SiteName = "Liqui",
                    NameExtended = string.Empty,
                    Last = m.Value.Last,
                    Ask = m.Value.Sell,
                    Bid = m.Value.Buy,
                    VolumeAsk = 0,
                    VolumeBid = 0,
                    ExchangeCoinSymbol = exch,
                    BaseCoinSymbol = bas
                });
            }
            testBase.db.SaveChanges();
        }
    }
}