﻿using System;
using System.Collections.Generic;
using System.Linq;
using CryptoFolio.Core;
using Xunit;

namespace CryptoFolio.Tests.LiquiDataTest
{
    public class ModifyOrderTests
    {
        private TestBaseLiqui testBase;
        private DatabaseApiCallsCaller handler;
        private ManageOrders manageOrders;
        private ManageElements manageElements;
        private ManagePortfolios managePortfolios;

        // Set-up database and load sample data
        public ModifyOrderTests()
        {
            testBase = new TestBaseLiqui("Offline");
            testBase.DbSetUpVoid();

            // Set up
            testBase.LoadCoins();
            testBase.LoadMarkets();

            manageOrders = new ManageOrders(testBase.log);
            manageElements = new ManageElements(testBase.log);
            managePortfolios = new ManagePortfolios();

            handler = new DatabaseApiCallsCaller(testBase);
        }

        [Fact]
        public void Quantity_Scenario1_FirstSecondCase()
        {
            /*** 1_1: 10 -> 5 ***/
            // Insert order
            var orders =
                testBase.LoadOrdersFile(
                    @"..\..\..\LiquiDataTest\Files\scenario_1.json");
            handler.InsertSampleOrders(orders);
            // Modify
            orders = testBase.LoadOrdersFile(
                @"..\..\..\LiquiDataTest\Files\Modify\Quantity\scenario_1_1.json");
            var oldOrder = testBase.db.Orders.Single(o => o.OrderId == 1);

            // Test
            Assert.Throws<Exception>(() => manageOrders.ModifyOrder(testBase.db, oldOrder, manageElements,
                managePortfolios, "Quantity", new List<object> {orders[0].Quantity}));

            // 1_2: 10 -> 8
            // Modify
            orders = testBase.LoadOrdersFile(
                @"..\..\..\LiquiDataTest\Files\Modify\Quantity\scenario_1_2.json");
            oldOrder = testBase.db.Orders.Single(o => o.OrderId == 2);

            manageOrders.ModifyOrder(testBase.db, oldOrder, manageElements, managePortfolios, "Quantity",
                new List<object> {orders[1].Quantity});
            managePortfolios.UpdatePortfolio(testBase.db, 1);
            testBase.db.SaveChanges();

            // Test
            var p = testBase.db.Portfolios.Single();
            var eth = testBase.db.Elements.Single(e => e.Coin.CoinSymbol.Equals("ETH"));
            var btc = testBase.db.Elements.Single(e => e.Coin.CoinSymbol.Equals("BTC"));

            Assert.Equal(8.84575738D, Math.Round(p.TotalValue, 8));

            Assert.Equal(2, eth.TotalQuantity);
            Assert.Equal(0.1D, Math.Round(eth.Costs[0].Cost, 1));
            Assert.Equal(2, eth.TotalValue);

            Assert.Equal(0.4, btc.TotalQuantity);
            Assert.Equal(6.84575738D, Math.Round(btc.TotalValue, 8));
            Assert.Equal(0, btc.Costs.Count);
        }

        [Fact]
        public void Quantity_Scenario1b_FirstCase()
        {
            /*** 1b_1: 10 -> 9 ***/
            // Insert order
            var orders =
                testBase.LoadOrdersFile(
                    @"..\..\..\LiquiDataTest\Files\scenario_1b.json");
            handler.InsertSampleOrders(orders);
            // Modify
            orders = testBase.LoadOrdersFile(
                @"..\..\..\LiquiDataTest\Files\Modify\Quantity\scenario_1b_1.json");

            // Modify
            var oldOrder = testBase.db.Orders.Single(o => o.OrderId == 1);

            manageOrders.ModifyOrder(testBase.db, oldOrder, manageElements, managePortfolios, "Quantity",
                new List<object> {orders[0].Quantity});
            managePortfolios.UpdatePortfolio(testBase.db, 1);
            testBase.db.SaveChanges();

            // Test
            var p = testBase.db.Portfolios.Single();
            var eth = testBase.db.Elements.Single(e => e.Coin.CoinSymbol.Equals("ETH"));
            var btc = testBase.db.Elements.Single(e => e.Coin.CoinSymbol.Equals("BTC"));

            Assert.Equal(7.84575738D, Math.Round(p.TotalValue, 8));

            Assert.Equal(1, eth.TotalQuantity);
            Assert.Equal(0.05D, Math.Round(eth.Costs[0].Cost, 2));
            Assert.Equal(1, eth.TotalValue);

            Assert.Equal(0.4, btc.TotalQuantity);
            Assert.Equal(6.84575738D, Math.Round(btc.TotalValue, 8));
            Assert.Equal(0, btc.Costs.Count);
        }

        [Fact]
        public void Quantity_Scenario1_ThirdCase()
        {
            /*** 1_3: 10 -> 11 ***/
            // Insert order
            var orders =
                testBase.LoadOrdersFile(
                    @"..\..\..\LiquiDataTest\Files\scenario_1.json");
            handler.InsertSampleOrders(orders);
            // Modify
            orders = testBase.LoadOrdersFile(
                @"..\..\..\LiquiDataTest\Files\Modify\Quantity\scenario_1_3.json");
            var oldOrder = testBase.db.Orders.Single(o => o.OrderId == 2);

            // Test
            Assert.Throws<Exception>(() => manageOrders.ModifyOrder(testBase.db, oldOrder, manageElements,
                managePortfolios, "Quantity", new List<object> {orders[1].Quantity}));
        }

        [Fact]
        public void Quantity_Scenario1_FourthCase()
        {
            /*** 1_4: 10 -> 11 ***/
            // Insert order
            var orders =
                testBase.LoadOrdersFile(
                    @"..\..\..\LiquiDataTest\Files\scenario_1.json");
            handler.InsertSampleOrders(orders);
            // Modify
            orders = testBase.LoadOrdersFile(
                @"..\..\..\LiquiDataTest\Files\Modify\Quantity\scenario_1_4.json");
            var oldOrder = testBase.db.Orders.Single(o => o.OrderId == 1);

            manageOrders.ModifyOrder(testBase.db, oldOrder, manageElements, managePortfolios, "Quantity",
                new List<object> {orders[0].Quantity});
            managePortfolios.UpdatePortfolio(testBase.db, 1);
            testBase.db.SaveChanges();

            // Test
            var p = testBase.db.Portfolios.Single();
            var eth = testBase.db.Elements.Single(e => e.Coin.CoinSymbol.Equals("ETH"));
            var btc = testBase.db.Elements.Single(e => e.Coin.CoinSymbol.Equals("BTC"));

            Assert.Equal(9.55719673D, Math.Round(p.TotalValue, 8));

            Assert.Equal(1, eth.TotalQuantity);
            Assert.Equal(0.05D, Math.Round(eth.Costs[0].Cost, 2));
            Assert.Equal(1, eth.TotalValue);

            Assert.Equal(0.5, btc.TotalQuantity);
            Assert.Equal(8.55719673D, Math.Round(btc.TotalValue, 8));
            Assert.Equal(0, btc.Costs.Count);
        }

        [Fact]
        public void Quantity_Scenario4_FirstCase()
        {
            /*** 4_1: 10 -> 8 ***/
            // Insert order
            var orders =
                testBase.LoadOrdersFile(
                    @"..\..\..\LiquiDataTest\Files\scenario_4.json");
            handler.InsertSampleOrders(orders);
            // Modify
            orders = testBase.LoadOrdersFile(
                @"..\..\..\LiquiDataTest\Files\Modify\Quantity\scenario_4_1.json");
            var oldOrder = testBase.db.Orders.Single(o => o.OrderId == 3);

            manageOrders.ModifyOrder(testBase.db, oldOrder, manageElements, managePortfolios, "Quantity",
                new List<object> {orders[2].Quantity});
            managePortfolios.UpdatePortfolio(testBase.db, 1);
            testBase.db.SaveChanges();

            // Test
            var p = testBase.db.Portfolios.Single();
            var eth = testBase.db.Elements.Single(e => e.Coin.CoinSymbol.Equals("ETH"));
            var btc = testBase.db.Elements.Single(e => e.Coin.CoinSymbol.Equals("BTC"));

            Assert.Equal(60.18893777D, Math.Round(p.TotalValue, 8));

            Assert.Equal(2, eth.TotalQuantity);
            Assert.Equal(0.1D, Math.Round(eth.Costs[0].Cost, 2));
            Assert.Equal(2, eth.TotalValue);

            Assert.Equal(3.4, btc.TotalQuantity);
            Assert.Equal(18000, btc.Costs[0].Cost);
            Assert.Equal(58.18893777D, Math.Round(btc.TotalValue, 8));
        }

        [Fact]
        public void Quantity_Scenario5_FirstCase()
        {
            /*** 5_1: 10 -> 11 ***/
            // Insert order
            var orders =
                testBase.LoadOrdersFile(
                    @"..\..\..\LiquiDataTest\Files\scenario_5.json");
            handler.InsertSampleOrders(orders);
            // Modify
            orders = testBase.LoadOrdersFile(
                @"..\..\..\LiquiDataTest\Files\Modify\Quantity\scenario_5_1.json");
            var oldOrder = testBase.db.Orders.Single(o => o.OrderId == 3);

            manageOrders.ModifyOrder(testBase.db, oldOrder, manageElements, managePortfolios, "Quantity",
                new List<object> {orders[2].Quantity});
            managePortfolios.UpdatePortfolio(testBase.db, 1);
            testBase.db.SaveChanges();

            // Test
            var p = testBase.db.Portfolios.Single();
            var eth = testBase.db.Elements.Single(e => e.Coin.CoinSymbol.Equals("ETH"));
            var btc = testBase.db.Elements.Single(e => e.Coin.CoinSymbol.Equals("BTC"));

            Assert.Equal(62.93026398D, Math.Round(p.TotalValue, 8));

            Assert.Equal(21, eth.TotalQuantity);
            Assert.Equal(1.05D, Math.Round(eth.Costs[0].Cost, 2));
            Assert.Equal(21, eth.TotalValue);

            Assert.Equal(2.45, btc.TotalQuantity);
            Assert.Equal(14700, btc.Costs[0].Cost);
            Assert.Equal(41.93026398D, Math.Round(btc.TotalValue, 8));
        }

        [Fact]
        public void Price_Scenario1_FirstCase()
        {
            /*** 1_1_price: 0.05->0.04 ***/
            // Insert order
            var orders =
                testBase.LoadOrdersFile(
                    @"..\..\..\LiquiDataTest\Files\scenario_1.json");
            handler.InsertSampleOrders(orders);
            // Modify
            orders = testBase.LoadOrdersFile(
                @"..\..\..\LiquiDataTest\Files\Modify\Price\scenario_1_1.json");
            var oldOrder = testBase.db.Orders.Single(o => o.OrderId == 1);

            manageOrders.ModifyOrder(testBase.db, oldOrder, manageElements, managePortfolios, "Price",
                new List<object> {orders[0].Price});
            managePortfolios.UpdatePortfolio(testBase.db, 1);
            testBase.db.SaveChanges();

            // Test
            var p = testBase.db.Portfolios.Single();
            var eth = testBase.db.Elements.Single(e => e.Coin.CoinSymbol.Equals("ETH"));
            var btc = testBase.db.Elements.Single(e => e.Coin.CoinSymbol.Equals("BTC"));

            Assert.Equal(8.55719673D, Math.Round(p.TotalValue, 8));

            Assert.Equal(0, eth.TotalQuantity);
            Assert.Equal(0, eth.Costs[0].Cost);
            Assert.Equal(0, eth.TotalValue);

            Assert.Equal(0.5, btc.TotalQuantity);
            Assert.Equal(8.55719673D, Math.Round(btc.TotalValue, 8));
            Assert.Equal(0, btc.Costs.Count);
        }

        [Fact]
        public void Price_Scenario12_FirstCase()
        {
            /*** 12_1_price: 6000->5000 ***/
            // Insert order
            var orders =
                testBase.LoadOrdersFile(
                    @"..\..\..\LiquiDataTest\Files\scenario_12.json");
            handler.InsertSampleOrders(orders);
            // Modify
            orders = testBase.LoadOrdersFile(
                @"..\..\..\LiquiDataTest\Files\Modify\Price\scenario_12_1.json");
            var oldOrder = testBase.db.Orders.Single(o => o.OrderId == 2);

            manageOrders.ModifyOrder(testBase.db, oldOrder, manageElements, managePortfolios, "Price",
                new List<object> { orders[1].Price });
            managePortfolios.UpdatePortfolio(testBase.db, 1);
            testBase.db.SaveChanges();

            // Test
            var p = testBase.db.Portfolios.Single();
            var usdt = testBase.db.Elements.Single(e => e.Coin.CoinSymbol.Equals("USDT"));
            var btc = testBase.db.Elements.Single(e => e.Coin.CoinSymbol.Equals("BTC"));

            Assert.Equal(71.34318038D, Math.Round(p.TotalValue, 8));            

            Assert.Equal(3, btc.TotalQuantity);
            Assert.Equal(51.34318038D, Math.Round(btc.TotalValue, 8));
            Assert.Equal(15000, btc.Costs[0].Cost);
        }

        [Fact]
        public void Price_Scenario8_FirstCase()
        {
            /*** 8_1_price: 0.0001->0.00015 ***/
            // Insert order
            var orders =
                testBase.LoadOrdersFile(
                    @"..\..\..\LiquiDataTest\Files\scenario_8.json");
            handler.InsertSampleOrders(orders);
            // Modify
            orders = testBase.LoadOrdersFile(
                @"..\..\..\LiquiDataTest\Files\Modify\Price\scenario_8_1.json");
            var oldOrder = testBase.db.Orders.Single(o => o.OrderId == 2);

            manageOrders.ModifyOrder(testBase.db, oldOrder, manageElements, managePortfolios, "Price",
                new List<object> {orders[1].Price});
            managePortfolios.UpdatePortfolio(testBase.db, 1);
            testBase.db.SaveChanges();

            // Test
            var p = testBase.db.Portfolios.Single();
            var eth = testBase.db.Elements.Single(e => e.Coin.CoinSymbol.Equals("ETH"));
            var btc = testBase.db.Elements.Single(e => e.Coin.CoinSymbol.Equals("BTC"));
            var dnt = testBase.db.Elements.Single(e => e.Coin.CoinSymbol.Equals("DNT"));

            Assert.Equal(9.61556787D, Math.Round(p.TotalValue, 8));

            Assert.Equal(8.8, eth.TotalQuantity);
            Assert.Equal(2550, Math.Round(eth.Costs[0].Cost, 0));
            Assert.Equal(8.8, eth.TotalValue);

            Assert.Equal(0.02, btc.TotalQuantity);
            Assert.Equal(0, btc.Costs.Count);
            Assert.Equal(0.34228787D, Math.Round(btc.TotalValue, 8));

            Assert.Equal(4000, dnt.TotalQuantity);
            Assert.Equal(0.6D, dnt.Costs[0].Cost);
            Assert.Equal(0.47328D, Math.Round(dnt.TotalValue, 5));
        }

        [Fact]
        public void QuantityPrice_Scenario1_FirstCase()
        {
            /*** 1_1: 0.05->0.04, 10->11 ***/
            // Insert order
            var orders =
                testBase.LoadOrdersFile(
                    @"..\..\..\LiquiDataTest\Files\scenario_1.json");
            handler.InsertSampleOrders(orders);
            // Modify
            orders = testBase.LoadOrdersFile(
                @"..\..\..\LiquiDataTest\Files\Modify\QuantityPrice\scenario_1_1.json");
            var oldOrder = testBase.db.Orders.Single(o => o.OrderId == 1);

            manageOrders.ModifyOrder(testBase.db, oldOrder, manageElements, managePortfolios, "Both",
                new List<object> {orders[0].Quantity, orders[0].Price});
            managePortfolios.UpdatePortfolio(testBase.db, 1);
            testBase.db.SaveChanges();

            // Test
            var p = testBase.db.Portfolios.Single();
            var eth = testBase.db.Elements.Single(e => e.Coin.CoinSymbol.Equals("ETH"));
            var btc = testBase.db.Elements.Single(e => e.Coin.CoinSymbol.Equals("BTC"));

            Assert.Equal(9.55719673D, Math.Round(p.TotalValue, 8));

            Assert.Equal(1, eth.TotalQuantity);
            Assert.Equal(0.04, Math.Round(eth.Costs[0].Cost, 2));
            Assert.Equal(1, eth.TotalValue);

            Assert.Equal(0.5, btc.TotalQuantity);
            Assert.Equal(0, btc.Costs.Count);
            Assert.Equal(8.55719673D, Math.Round(btc.TotalValue, 8));
        }

        [Fact]
        public void Type_Scenario5_FirstCase()
        {
            /*** 5_1: Buy -> Sell (terzo ordine) ***/
            // Insert order
            var orders =
                testBase.LoadOrdersFile(
                    @"..\..\..\LiquiDataTest\Files\scenario_5.json");
            handler.InsertSampleOrders(orders);
            // Modify
            orders = testBase.LoadOrdersFile(
                @"..\..\..\LiquiDataTest\Files\Modify\Type\scenario_5_1.json");
            var oldOrder = testBase.db.Orders.Single(o => o.OrderId == 3);

            manageOrders.ModifyOrder(testBase.db, oldOrder, manageElements, managePortfolios, "Type",
                new List<object> { });
            managePortfolios.UpdatePortfolio(testBase.db, 1);
            testBase.db.SaveChanges();

            // Test
            var p = testBase.db.Portfolios.Single();
            var eth = testBase.db.Elements.Single(e => e.Coin.CoinSymbol.Equals("ETH"));
            var btc = testBase.db.Elements.Single(e => e.Coin.CoinSymbol.Equals("BTC"));

            Assert.Equal(59.90037712D, Math.Round(p.TotalValue, 8));

            Assert.Equal(0, eth.TotalQuantity);
            Assert.Equal(0, Math.Round(eth.Costs[0].Cost, 0));
            Assert.Equal(0, eth.TotalValue);

            Assert.Equal(3.5, btc.TotalQuantity);
            Assert.Equal(18000, btc.Costs[0].Cost);
            Assert.Equal(59.90037712D, Math.Round(btc.TotalValue, 8));
        }

        [Fact]
        public void Oldfunds_Scenario8_FirstCase()
        {
            /*** 8_1_price: 0.0001->0.00015 ***/
            // Insert order
            var orders =
                testBase.LoadOrdersFile(
                    @"..\..\..\LiquiDataTest\Files\scenario_8.json");
            handler.InsertSampleOrders(orders);
            // Modify
            orders = testBase.LoadOrdersFile(
                @"..\..\..\LiquiDataTest\Files\Modify\Oldfunds\scenario_8_1.json");
            var oldOrder = testBase.db.Orders.Single(o => o.OrderId == 2);

            manageOrders.ModifyOrder(testBase.db, oldOrder, manageElements, managePortfolios, "Oldfunds",
                new List<object> { });
            managePortfolios.UpdatePortfolio(testBase.db, 1);
            //testBase.db.SaveChanges();

            // Test
            var p = testBase.db.Portfolios.Single();
            var eth = testBase.db.Elements.Single(e => e.Coin.CoinSymbol.Equals("ETH"));
            var btc = testBase.db.Elements.Single(e => e.Coin.CoinSymbol.Equals("BTC"));
            var dnt = testBase.db.Elements.Single(e => e.Coin.CoinSymbol.Equals("DNT"));

            Assert.Equal(11.11556787D, Math.Round(p.TotalValue, 8));

            Assert.Equal(10.3, eth.TotalQuantity);
            Assert.Equal(3000, Math.Round(eth.Costs[0].Cost, 0));
            Assert.Equal(10.3, eth.TotalValue);

            Assert.Equal(0.02, btc.TotalQuantity);
            Assert.Equal(0, btc.Costs.Count);
            Assert.Equal(0.34228787D, Math.Round(btc.TotalValue, 8));

            Assert.Equal(4000, dnt.TotalQuantity);
            Assert.Equal(0.4D, dnt.Costs[0].Cost);
            Assert.Equal(0.47328D, Math.Round(dnt.TotalValue, 5));
        }
    }
}