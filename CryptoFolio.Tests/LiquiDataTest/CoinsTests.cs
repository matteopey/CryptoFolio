﻿using System;
using Xunit;
using System.Linq;

namespace CryptoFolio.Tests.LiquiDataTest
{
    public class CoinsTests
    {
        private TestBaseLiqui testBase;

        // Set-up database and load sample data
        public CoinsTests()
        {
            testBase = new TestBaseLiqui("Offline");
            testBase.DbSetUpVoid();

            // Set up
            testBase.LoadCoins();
        }

        [Fact]
        public void DbDeleteCoin_ExistingCoin_ShouldReturn1()
        {
            var coin = testBase.db.Coins.Single(c => c.CoinSymbol.Equals("BTC"));

            // Delete            
            testBase.db.Coins.Remove(coin);
            var res = testBase.db.SaveChanges();

            Assert.Equal(1, res);
        }

        [Fact]
        public void DbGetCoin_NonExistingCoin_ShouldThrowException()
        {
            // Delete           
            Assert.Throws<InvalidOperationException>(() =>
            {
                testBase.db.Coins.Single(c => c.CoinSymbol.Equals("FakeCoin"));
                testBase.db.SaveChanges();
            });
        }
    }
}