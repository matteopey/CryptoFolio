﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using CryptoFolio.Core;
using CryptoFolio.Core.SQLite.Model;
using Xunit;

namespace CryptoFolio.Tests.LiquiDataTest
{
    public class TrendTests
    {
        private TestBaseLiqui testBase;
        private DatabaseApiCallsCaller handler;

        // Set-up database and load sample data
        public TrendTests()
        {
            testBase = new TestBaseLiqui("Offline");
            testBase.DbSetUpVoid();

            // Set up
            testBase.LoadCoins();
            testBase.LoadMarkets();

            handler = new DatabaseApiCallsCaller(testBase);
        }

        [Fact]
        public void ElementIsReferenceCoin_Scenario1()
        {
            // Insert orders
            var orders =
                testBase.LoadOrdersFile(
                    @"..\..\..\LiquiDataTest\Files\Trend\scenario_1.json");
            handler.InsertSampleOrders(orders);

            // Test trend
            var eth = testBase.db.Elements.Single(e => e.CoinSymbol.Equals("ETH"));

            handler.SetHistoricalData(testBase.db, eth, CandleFactory.CreateCandle("1H"));
            handler.GenerateTrend(testBase.db, eth, handler.hourFactory).Wait();

            // Time passed between now and the date of the first order.
            var timespanSeconds = Utils.ToUnixTime(DateTime.UtcNow) - Utils.ToUnixTime(eth.Trend[0].Time);
            var timespan = Math.Ceiling((double)timespanSeconds / 60 / 60); // Hours. 

            Assert.Equal(timespan, eth.Trend.Count);
            Assert.Equal(DateTime.Parse("2018-04-01T00:00Z").ToUniversalTime(), eth.Trend.First().Time);
            Assert.Equal(10, eth.Trend.First().Value);
        }

        [Fact]
        public void SimpleMarket_Scenario1()
        {
            // Insert orders
            var orders =
                testBase.LoadOrdersFile(
                    @"..\..\..\LiquiDataTest\Files\Trend\scenario_1.json");
            handler.InsertSampleOrders(orders);

            // Test trend

            var btc = testBase.db.Elements.Single(e => e.CoinSymbol.Equals("BTC"));

            handler.SetHistoricalData(testBase.db, btc, CandleFactory.CreateCandle("1H"));
            handler.GenerateTrend(testBase.db, btc, handler.hourFactory).Wait();

            // Time passed between now and the date of the first order.
            var timespanSeconds = Utils.ToUnixTime(DateTime.UtcNow) - Utils.ToUnixTime(btc.Trend[0].Time);
            var timespan = Math.Ceiling((double)timespanSeconds / 60 / 60); // Hours. 

            Assert.Equal(timespan, btc.Trend.Count);
        }

        [Fact]
        public void MarketCombination_Scenario2()
        {
            // The Element coin is not exchanged with the reference coin.
            // The value of the element is determined by the exchange between the element coin with BTC 
            // and the reference with BTC.

            // Change reference coin to ZRX
            testBase.db.Portfolios.Single().Coin = testBase.db.Coins.Single(c => c.CoinSymbol.Equals("ZRX"));

            // Insert orders
            var orders =
                testBase.LoadOrdersFile(
                    @"..\..\..\LiquiDataTest\Files\Trend\scenario_2.json");
            handler.InsertSampleOrders(orders);

            // Test trend
            var ltc = testBase.db.Elements.Single(e => e.CoinSymbol.Equals("LTC"));

            handler.SetHistoricalData(testBase.db, ltc, CandleFactory.CreateCandle("1H"));
            handler.GenerateTrend(testBase.db, ltc, handler.hourFactory).Wait();

            // Time passed between now and the date of the first order.
            var timespanSeconds = Utils.ToUnixTime(DateTime.UtcNow) - Utils.ToUnixTime(ltc.Trend[0].Time);
            var timespan = Math.Ceiling((double)timespanSeconds / 60 / 60); // Hours. 

            Assert.Equal(timespan, ltc.Trend.Count);
            Assert.Equal(DateTime.UtcNow.Hour, ltc.Trend.Last().Time.Hour);

            Assert.InRange(ltc.Trend.Last().Value, 1000, 10000);
        }

        [Fact]
        public void SimpleMarket_Day_Scenario3()
        {
            // Insert orders
            var orders =
                testBase.LoadOrdersFile(
                    @"..\..\..\LiquiDataTest\Files\Trend\scenario_3.json");
            handler.InsertSampleOrders(orders);

            var btc = testBase.db.Elements.Single(e => e.CoinSymbol.Equals("BTC"));

            handler.SetHistoricalData(testBase.db, btc, CandleFactory.CreateCandle("1D"));
            handler.GenerateTrend(testBase.db, btc, handler.dayFactory).Wait();

            // Time passed between now and the date of the first order.
            var timespanSeconds =
                Utils.ToUnixTime(DateTime.UtcNow) - Utils.ToUnixTime(btc.Trend[0].Time);
            var timespan = Math.Ceiling((double)timespanSeconds / (24 * 60 * 60)); // Days. 

            Assert.Equal(timespan, btc.Trend.Count);
        }

        [Fact]
        public void SimpleMarket_Scenario1_4H()
        {
            // Insert orders
            var orders =
                testBase.LoadOrdersFile(
                    @"..\..\..\LiquiDataTest\Files\Trend\scenario_1.json");
            handler.InsertSampleOrders(orders);

            // Test trend

            var btc = testBase.db.Elements.Single(e => e.CoinSymbol.Equals("BTC"));

            var factory = new TrendFactoryHour(handler.manageElements);

            handler.SetHistoricalData(testBase.db, btc, CandleFactory.CreateCandle("4H"));
            handler.GenerateTrend(testBase.db, btc, factory).Wait();

            // Time passed between now and the date of the first order.
            var timespanSeconds = Utils.ToUnixTime(DateTime.UtcNow) - Utils.ToUnixTime(btc.Trend[0].Time);
            var timespan = Math.Ceiling((double)timespanSeconds / 60 / 60 / 4); // 4 hours. 

            Assert.Equal(timespan, btc.Trend.Count);
        }

        [Fact]
        public void SimpleMarket_Scenario1_1W()
        {
            // Insert orders
            var orders =
                testBase.LoadOrdersFile(
                    @"..\..\..\LiquiDataTest\Files\Trend\scenario_1.json");
            handler.InsertSampleOrders(orders);

            // Test trend

            var btc = testBase.db.Elements.Single(e => e.CoinSymbol.Equals("BTC"));

            handler.SetHistoricalData(testBase.db, btc, CandleFactory.CreateCandle("1W"));
            var factory = new TrendFactoryDay(handler.manageElements);

            handler.GenerateTrend(testBase.db, btc, factory).Wait();

            // Time passed between now and the date of the first order.
            var timespanSeconds = Utils.ToUnixTime(DateTime.UtcNow) - Utils.ToUnixTime(btc.Trend[0].Time);
            var timespan = Math.Ceiling((double)timespanSeconds / (24 * 60 * 60 * 7)); // 7 days. 

            Assert.Equal(timespan, btc.Trend.Count);
        }

        [Fact]
        public void ElementIsReferenceCoin_Scenario1_1W()
        {
            // Insert orders
            var orders =
                testBase.LoadOrdersFile(
                    @"..\..\..\LiquiDataTest\Files\Trend\scenario_1.json");
            handler.InsertSampleOrders(orders);

            // Test trend
            var eth = testBase.db.Elements.Single(e => e.CoinSymbol.Equals("ETH"));

            var factory = new TrendFactoryDay(handler.manageElements);

            handler.SetHistoricalData(testBase.db, eth, CandleFactory.CreateCandle("1W"));
            handler.GenerateTrend(testBase.db, eth, factory).Wait();

            // Time passed between now and the date of the first order.
            var timespanSeconds = Utils.ToUnixTime(DateTime.UtcNow) - Utils.ToUnixTime(eth.Trend[0].Time);
            var timespan = Math.Ceiling((double)timespanSeconds / (24 * 60 * 60 * 7)); // 7 days. 

            Assert.Equal(timespan, eth.Trend.Count);
            Assert.Equal(DateTime.Parse("2018-04-01T00:00Z").ToUniversalTime(), eth.Trend.First().Time);
            Assert.Equal(10, eth.Trend.First().Value);
        }

        [Fact]
        public void MarketCombination_Scenario2_1W()
        {
            // The Element coin is not exchanged with the reference coin.
            // The value of the element is determined by the exchange between the element coin with BTC 
            // and the reference with BTC.

            // Change reference coin to ZRX
            testBase.db.Portfolios.Single().Coin = testBase.db.Coins.Single(c => c.CoinSymbol.Equals("ZRX"));

            // Insert orders
            var orders =
                testBase.LoadOrdersFile(
                    @"..\..\..\LiquiDataTest\Files\Trend\scenario_2.json");
            handler.InsertSampleOrders(orders);

            // Test trend
            var ltc = testBase.db.Elements.Single(e => e.CoinSymbol.Equals("LTC"));
            var factory = new TrendFactoryDay(handler.manageElements);

            handler.SetHistoricalData(testBase.db, ltc, CandleFactory.CreateCandle("1W"));
            handler.GenerateTrend(testBase.db, ltc, factory).Wait();

            // Time passed between now and the date of the first order.
            var timespanSeconds = Utils.ToUnixTime(DateTime.UtcNow) - Utils.ToUnixTime(ltc.Trend[0].Time);
            var timespan = Math.Ceiling((double)timespanSeconds / (24 * 60 * 60 * 7)); // 7 days. 

            Assert.Equal(timespan, ltc.Trend.Count);

            Assert.InRange(ltc.Trend.Last().Value, 1000, 10000);
        }
    }
}