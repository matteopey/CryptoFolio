﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CryptoFolio.Core;
using CryptoFolio.Core.Exceptions;
using CryptoFolio.Core.SQLite;
using CryptoFolio.Core.SQLite.Model;
using CryptoFolio.Core.UIClassesModel;

namespace CryptoFolio.Tests.LiquiDataTest
{
    public class DatabaseApiCallsCaller
    {
        private TestBaseLiqui testBase;
        private ManageOrders manageOrders;
        public ManageElements manageElements;
        private ManagePortfolios managePortfolios = new ManagePortfolios();

        public TrendFactory dayFactory;
        public TrendFactory hourFactory;

        public DatabaseApiCallsCaller(TestBaseLiqui testBase)
        {
            this.testBase = testBase;

            manageOrders = new ManageOrders(testBase.log);
            manageElements = new ManageElements(testBase.log);

            dayFactory = new TrendFactoryDay(manageElements);
            hourFactory = new TrendFactoryHour(manageElements);
        }

        public void InsertSampleOrders(List<UIOrder> orders)
        {
            foreach (var o in orders)
            {
                try
                {
                    o.SiteName = "Liqui";
                    var dbOrder = manageOrders.CreateOrder(testBase.db, o);
                    manageElements.UpdateElements(testBase.db, dbOrder);
                    managePortfolios.UpdatePortfolio(testBase.db, 1);
                }
                catch (ElementException e)
                {
                    throw e;
                }
            }
        }

        public async Task GenerateTrend(PortfolioContext db, Element el, TrendFactory builder)
        {
            await builder.GenerateTrend(db, el);
        }

        public void SetHistoricalData(PortfolioContext db, Element el, Candle candle)
        {
            manageElements.SetHistoricalData(db, el, candle);
        }
    }
}
