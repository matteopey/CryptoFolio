# CryptoFolio

## A software for tracking cryptocurrencies balances.

### Usage
There is a very basic console interface in **CryptoFolio.Console** 
[here](../CryptoFolio.Console)

Follow the instructions on README to use it.