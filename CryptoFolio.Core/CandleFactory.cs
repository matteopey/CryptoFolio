﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CryptoFolio.Core
{
    public class CandleFactory
    {
        private const int HourUnit = 60 * 60;
        private const int DayUnit = 24 * 60 * 60;

        public static Candle CreateCandle(string unit)
        {
            switch (unit)
            {
                case "1H":
                    return new Candle(HourUnit, 1);
                case "2H":
                    return new Candle(HourUnit, 2);
                case "4H":
                    return new Candle(HourUnit, 4);
                case "1D":
                    return new Candle(DayUnit, 1);
                case "2D":
                    return new Candle(DayUnit, 2);
                case "4D":
                    return new Candle(DayUnit, 4);
                case "1W":
                    return new Candle(DayUnit, 7);
                default:
                    throw new Exception("Didn't specify candle dimension.");
            }
        }
    }
}
