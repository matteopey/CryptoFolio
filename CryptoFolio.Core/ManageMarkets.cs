﻿using System.Text;
using CryptoFolio.Core.SQLite;

namespace CryptoFolio.Core
{
    public class ManageMarkets
    {
        public string PrintMarkets(PortfolioContext db)
        {
            var builder = new StringBuilder();
            builder.AppendLine("**** MARKETS ****");
            foreach (var m in db.Markets)
            {
                builder.AppendLine(m.Name);
                builder.AppendLine("Keys: ");
                builder.AppendFormat("\t{0}\n", m.ExchangeCoinSymbol);
                builder.AppendFormat("\t{0}\n", m.BaseCoinSymbol);
                builder.AppendFormat("\t{0}\n", m.SiteName);
                builder.AppendFormat("Last: {0,-1}", m.Last);
                builder.AppendFormat("Ask: {0,10}", m.Ask);
                builder.AppendFormat("Bid: {0,10}", m.Bid);
                builder.AppendLine("\n");
            }
            return builder.ToString();
        }
    }
}