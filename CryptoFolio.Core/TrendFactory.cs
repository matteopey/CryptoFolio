﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using CryptoFolio.Core.SQLite;
using CryptoFolio.Core.SQLite.Model;

namespace CryptoFolio.Core
{
    public abstract class TrendFactory
    {
        public abstract Task GenerateTrend(PortfolioContext db, Element el);
        
        // Function that is common to every implementations
        public List<Tick> BuildQuantityTrend(List<Order> orders, Element el)
        {
            // Quantity represents the total quantity of the element 
            // when a specific order happened.
            double quantity = 0;
            var trend = new List<Tick>();

            foreach (var order in orders)
            {
                // Update the total quantity of the element correctly
                if (order.Type.Equals("Buy"))
                {
                    if (order.ExchangeCoinSymbol.Equals(el.CoinSymbol))
                        quantity += order.Quantity;
                    else
                        quantity -= order.Cost;
                }
                else
                {
                    if (order.ExchangeCoinSymbol.Equals(el.CoinSymbol))
                        quantity -= order.Quantity;
                    else
                        quantity += order.Cost;
                }

                trend.Add(new Tick
                {
                    Time = order.Date,
                    Value = quantity
                });
            }

            return trend;
        }
    }
}
