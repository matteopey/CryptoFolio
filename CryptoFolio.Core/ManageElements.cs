﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading.Tasks.Dataflow;
using CryptoFolio.Core.Exceptions;
using CryptoFolio.Core.SQLite;
using CryptoFolio.Core.SQLite.Model;
using ExchangesApi;
using Microsoft.EntityFrameworkCore;

namespace CryptoFolio.Core
{
    public class ManageElements
    {
        private BufferBlock<string> log;

        public ManageElements(BufferBlock<string> logger)
        {
            log = logger;
        }

        public Element CreateElement(PortfolioContext db, Coin coin)
        {
            var el = new Element
            {
                Coin = coin,
                Portfolio = db.Portfolios.Single(),
                Costs = new List<ElementCost>(),
                TotalQuantity = 0,
                TotalValue = 0,
                TotalCost = 0,
                ElementOrders = new List<ElementOrder>()
            };

            db.Elements.Add(el);

            return el;
        }

        // TODO: OnAddOrder
        // Update the elements according the new order
        public Element UpdateElements(PortfolioContext db, Order order)
        {
            var exchangeCoin = order.Market.ExchangeCoin;
            var baseCoin = order.Market.BaseCoin;

            var elements = db.Elements.Include(e => e.Costs).ToList();

            var exchEl = elements.Single(e => e.Coin == exchangeCoin);
            var baseEl = elements.Single(e => e.Coin == baseCoin);
            var site = db.Sites.Single(s => s.SiteName.Equals(order.SiteName));

            switch (order.Type)
            {               
                case "Buy":
                    // Trying to buy with funds that you don't have
                    if (order.OldFunds && order.Cost > baseEl.TotalQuantity)
                    {
                        throw new ElementException();
                    }

                    exchEl.TotalQuantity += order.Quantity;

                    // Search the right cost
                    // Get element costs
                    try
                    {
                        // Find the right cost
                        var elCost = db.ElementCosts.Single(ec => ec.Element == exchEl && ec.BaseCoin == baseCoin);
                        elCost.Cost += order.Cost;
                        //elCost.Cost = exchEl.TotalQuantity * elCost.Price;
                        //if (exchEl.TotalQuantity == 0)
                        //	elCost.Cost = 0;
                        //else
                        //	elCost.Price = elCost.Cost / exchEl.TotalQuantity;
                    }
                    catch (Exception e)
                    {
                        log.SendAsync("Element cost not found => creating new");
                        exchEl.Costs.Add(new ElementCost
                        {
                            Element = exchEl,
                            Portfolio = exchEl.Portfolio,
                            CoinSymbol = exchEl.CoinSymbol,
                            BaseCoinSymbol = baseCoin.CoinSymbol,
                            BaseCoin = baseCoin,
                            Cost = order.Cost,
                            Price = order.Cost / exchEl.TotalQuantity
                        });
                    }
                                        
                    UpdateValue(db, exchEl, site);

                    if (order.OldFunds)
                    {
                        // Check the balance for base element is > 0
                        if (baseEl.TotalQuantity == 0)
                        {
                            throw new ElementException();
                        }

                        // Need to update the BaseCoin element
                        var oldQuantity = baseEl.TotalQuantity;

                        baseEl.TotalQuantity -= order.Cost;

                        // Multiplicator factor for cost
                        var f = 1 - order.Cost / oldQuantity;

                        // TODO: is this good? 
                        // Update first element. Whatever happens.                            
                        if (baseEl.Costs.Count != 0)
                            baseEl.Costs[0].Cost *= f;                        
                        //baseEl.Costs[0].Price = baseEl.Costs[0].Cost / baseEl.TotalQuantity;
                                                
                        UpdateValue(db, baseEl, site);
                    }
                    break;

                case "Sell":
                    // Selling something you don't have
                    if (exchEl.TotalQuantity < order.Quantity)
                    {
                        throw new ElementException();
                    }

                    var proceeds = order.Cost;

                    //Multiplicator factor for cost

                    var factor = 1 - order.Quantity / exchEl.TotalQuantity;

                    // TODO: is this good? 
                    exchEl.Costs[0].Cost *= factor;

                    exchEl.TotalQuantity -= order.Quantity;

                    //exchEl.Costs[0].Price = exchEl.Costs[0].Cost / exchEl.TotalQuantity;

                    baseEl.TotalQuantity += proceeds;
                    //baseEl.Costs[0].Price = baseEl.Costs[0].Cost / baseEl.TotalQuantity;
                                        
                    UpdateValue(db, exchEl, site);
                    UpdateValue(db, baseEl, site);
                    break;

                default: throw new ElementException();
            }

            db.SaveChanges();

            return null;
        }

        // TODO: considerare se usare questa versione per i casi in cui è possibile applicarla.
        public void UpdateElementsOnModifyOrder(PortfolioContext db, Order oldOrder, Order updatedOrder,
            string modifier)
        {
            // Take the two elements
            var exchEl = db.Elements.Single(e => e.Coin == oldOrder.Market.ExchangeCoin);
            var baseEl = db.Elements.Single(e => e.Coin == oldOrder.Market.BaseCoin);
            switch (modifier)
            {
                case "Quantity":
                    if (oldOrder.Type.Equals("Sell"))
                    {
                        /* Undo oldOrder */
                        exchEl.TotalQuantity += oldOrder.Quantity;
                        var elCost = db.ElementCosts.Single(ec => ec.CoinSymbol.Equals(exchEl.CoinSymbol));
                        elCost.Cost = exchEl.TotalQuantity * elCost.Price;

                        baseEl.TotalQuantity -= oldOrder.Cost;
                    }
                    else
                    {
                        /* Undo oldOrder */
                        exchEl.TotalQuantity -= oldOrder.Quantity;
                        var elCost = db.ElementCosts.Single(ec => ec.CoinSymbol.Equals(exchEl.CoinSymbol));
                        elCost.Cost = exchEl.TotalQuantity * elCost.Price;

                        if (oldOrder.OldFunds)
                        {
                            baseEl.TotalQuantity += oldOrder.Cost;
                            // Update cost
                            //TODO: bad
                            baseEl.Costs[0].Cost = baseEl.TotalQuantity * baseEl.Costs[0].Price;
                        }
                    }
                    /* Update the elements like you are adding a new order */
                    UpdateElements(db, updatedOrder);
                    break;
                case "Price":
                    if (oldOrder.Type.Equals("Sell"))
                    {
                        /* Undo oldOrder */
                        exchEl.TotalQuantity += oldOrder.Quantity;
                        var elCost = db.ElementCosts.Single(ec => ec.CoinSymbol.Equals(exchEl.CoinSymbol));
                        elCost.Cost = exchEl.TotalQuantity * elCost.Price;

                        baseEl.TotalQuantity -= oldOrder.Cost;
                    }
                    else
                    {
                        /* Undo oldOrder */
                        exchEl.TotalQuantity -= oldOrder.Quantity;
                        var elCost = db.ElementCosts.Single(ec => ec.CoinSymbol.Equals(exchEl.CoinSymbol));
                        elCost.Cost = exchEl.TotalQuantity * elCost.Price;

                        if (oldOrder.OldFunds)
                        {
                            baseEl.TotalQuantity += oldOrder.Cost;
                            // Update cost
                            //TODO: bad
                            baseEl.Costs[0].Cost = baseEl.TotalQuantity * baseEl.Costs[0].Price;
                        }
                    }
                    /* Update the elements like you are adding a new order */
                    UpdateElements(db, updatedOrder);
                    break;
            }

            // Delete order
            // Delete element
        }

        public void UpdateElementsOnModifyOrderV2(PortfolioContext db, Order newOrder)
        {         
            var exchEl = db.Elements.Single(e => e.Coin == newOrder.Market.ExchangeCoin);
            var baseEl = db.Elements.Single(e => e.Coin == newOrder.Market.BaseCoin);            

            UpdateElementsV2(db, exchEl, newOrder.Market.Site);
            UpdateElementsV2(db, baseEl, newOrder.Market.Site);

            db.SaveChanges();
        }

        private void UpdateElementsV2(PortfolioContext db, Element el, Site site)
        {
            // Take all orders where the element coin is present
            // Sort the by date
            var orders = db.ElementOrders.Where(eo => eo.Element == el).OrderBy(eo => eo.Order.Date).ToList();

            double quantity = 0;            
            var costs = db.ElementCosts.Where(ec => ec.Element == el).ToDictionary(c => c.BaseCoin);

            // Initialize dictionary with cost == 0
            foreach (var pair in costs)
            {
                pair.Value.Cost = 0;
            }

            foreach (var o in orders)
            {
                // If the element coin considered is Exchange coin for this order
                if (o.Order.Market.ExchangeCoin == el.Coin)
                {
                    if (o.Order.Type.Equals("Buy"))
                    {
                        // Sum quantity
                        quantity += o.Order.Quantity;

                        // Sum the right cost
                        // Check if the cost is present
                        if (costs.ContainsKey(o.Order.Market.BaseCoin))
                            costs[o.Order.Market.BaseCoin].Cost += o.Order.Cost;
                        else
                        {
                            // Create new cost
                            var ec = new ElementCost()
                            {
                                Element = el,
                                Portfolio = el.Portfolio,
                                CoinSymbol = el.CoinSymbol,
                                BaseCoinSymbol = o.Order.Market.BaseCoin.CoinSymbol,
                                BaseCoin = o.Order.Market.BaseCoin,
                                Cost = o.Order.Cost,
                                Price = 0
                            };

                            // Add new cost on database
                            el.Costs.Add(ec);

                            // Add new cost to dictionary 
                            costs.Add(ec.BaseCoin, ec);
                        }
                    }
                    else
                    {
                        // Sell
                        // Selling something you don't have
                        if (quantity < o.Order.Quantity)
                        {
                            throw new ElementException("You are trying to sell something you don't have");
                        }

                        var f = 1 - o.Order.Quantity / quantity;

                        // TODO: is this good? 
                        //exchEl.Costs[0].Cost *= factor;
                        costs.First().Value.Cost *= f;

                        quantity -= o.Order.Quantity;
                    }
                }
                else
                {
                    if (o.Order.Type.Equals("Buy"))
                    {
                        if (o.Order.OldFunds)
                        {
                            // Trying to buy with funds that you don't have
                            if (o.Order.Cost > quantity)
                            {
                                throw new ElementException("You are trying to buy with funds that you don't have");
                            }

                            var oldQuantity = quantity;

                            quantity -= o.Order.Cost;

                            // Multiplicator factor for cost
                            var f = 1 - o.Order.Cost / oldQuantity;

                            // TODO: is this good? 
                            // Update first element. Whatever happens.                            
                            //baseEl.Costs[0].Cost *= f;
                            costs.First().Value.Cost *= f;
                        }
                    }
                    else
                    {
                        quantity += o.Order.Cost;
                    }
                }
            }

            // Update element on db
            el.TotalQuantity = quantity;
            var element = db.Elements.Include(e => e.Costs).Where(e => e == el).Single();
            foreach (var cost in element.Costs)
            {
                cost.Cost = costs[cost.BaseCoin].Cost;
            }

            // Update element value
            UpdateValue(db, el, site);
        }

        public void UpdateValue(PortfolioContext db, Element el, Site site)
        {
            // Reference coin
            var refCoin = db.Portfolios.Include(p => p.Coin).Single().Coin;      

            // If the element coin is the reference coin, value == quantity
            // eg. coin=ETH, ref=ETH
            if (el.Coin == refCoin)
            {
                el.TotalValue = el.TotalQuantity;
                return;
            }

            // Find Coin-Reference Market
            // eg. ZRX-ETH, ZRX-BTC
            var market = db.Markets.Where(m => m.ExchangeCoin == el.Coin &&
                                               m.BaseCoin == refCoin &&
                                               m.Site.Equals(site))
                .ToList();

            // Find Reference-Coin Market
            // Useful in case of dealing with USDT which is never an exchange coin
            // eg. elementCoin=USDT => we need USDT-ETH market
            if (market.Count == 0)
            {
                // Find Reference-Coin Market
                market = db.Markets.Where(m => m.ExchangeCoin == refCoin &&
                                               m.BaseCoin == el.Coin &&
                                               m.Site.Equals(site))
                    .ToList();

                if (market.Count != 0)
                {
                    var lastPrice = market[0].Last;

                    // Divide the price
                    el.TotalValue = el.TotalQuantity / lastPrice;

                    return;
                }
            }

            // Should never happen
            if (market.Count > 1)
            {
                throw new ElementException();
            }

            // Coin-Reference Market doesn't exist
            if (market.Count == 0)
            {
                // Find Coin-BTC Market
                market = db.Markets.Where(m => m.ExchangeCoin == el.Coin &&
                                               m.BaseCoinSymbol.Equals("BTC") &&
                                               m.Site.Equals(site))
                    .ToList();
                var lastPrice = market[0].Last;

                // Find Reference-BTC Market
                var refBtc = db.Markets.Single(m => m.ExchangeCoin == refCoin &&
                                                    m.BaseCoinSymbol.Equals("BTC") &&
                                                    m.Site.Equals(site));

                el.TotalValue = el.TotalQuantity * lastPrice / refBtc.Last;
            }
            else
            {
                var lastPrice = market[0].Last;
                el.TotalValue = el.TotalQuantity * lastPrice;
            }
        }

        public void GenerateTrend(PortfolioContext db, Element el, TrendFactory trendFactory)
        {
            trendFactory.GenerateTrend(db, el);
        }

        public (Maybe<Market> m1, Maybe<Market> m2) FindMarket(PortfolioContext db, Element el)
        {
            // Reference coin
            var refCoin = db.Portfolios.Single().Coin;

            // If the element coin is the reference coin, value == quantity
            // eg. coin=ETH, ref=ETH
            if (el.Coin == refCoin)
                return (new Maybe<Market>(), new Maybe<Market>());

            // Find Coin-Reference Market
            // eg. ZRX-ETH, ZRX-BTC
            var market = db.Markets.Where(m => m.ExchangeCoin == el.Coin &&
                                               m.BaseCoin == refCoin)
                .ToList();

            if (market.Count > 0)
                return (m1: new Maybe<Market>(market[0]), new Maybe<Market>());

            // Find Reference-Coin Market
            // Useful in case of dealing with USDT which is never an exchange coin
            // eg. elementCoin=USDT => we need USDT-ETH market
            if (market.Count == 0)
            {
                // Find Reference-Coin Market
                market = db.Markets.Where(m => m.ExchangeCoin == refCoin &&
                                               m.BaseCoin == el.Coin)
                    .ToList();
            }

            if (market.Count > 0)
                return (m1: new Maybe<Market>(market[0]), new Maybe<Market>());

            // Coin-Reference Market doesn't exist
            if (market.Count == 0)
            {
                // Find Coin-BTC Market
                market = db.Markets.Where(m => m.ExchangeCoin == el.Coin &&
                                               m.BaseCoinSymbol.Equals("BTC"))
                    .ToList();

                // Find Reference-BTC Market
                var refBtc = db.Markets.Single(m => m.ExchangeCoin == refCoin &&
                                                    m.BaseCoinSymbol.Equals("BTC"));

                return (m1: new Maybe<Market>(market[0]), m2: new Maybe<Market>(refBtc));
            }

            throw new Exception("Cant't find proper markets, check your element.");
        }

        public void UpdateElementsValue(PortfolioContext db)
        {
            var elements = db.Elements
                .Include(el => el.Coin)
                .Include(el => el.ElementOrders)
                .ToList();           

            foreach (var dbElement in elements)
            {
                // If there are no orders, the total value is 0
                // we continue the loop otherwise the next part throws
                if (dbElement.ElementOrders.Count == 0)
                {
                    dbElement.TotalValue = 0;
                    continue;
                }

                var siteName = db.ElementOrders
                    .Include(eo => eo.Order)
                    .Where(eo => eo.Element == dbElement)
                    .First()
                    .Order
                    .SiteName;                    
                
                var site = db.Sites.Single(s => s.SiteName.Equals(siteName));

                UpdateValue(db, dbElement, site);
            }

            db.SaveChanges();
        }

        public void SetHistoricalData(PortfolioContext db, Element el, Candle candle)
        {
            el.Candle = candle;

            db.SaveChanges();
        }

        public string PrintElement(Element el)
        {
            var builder = new StringBuilder();

            builder.AppendLine("**** ELEMENT ****");
            builder.AppendLine(el.Coin.CoinSymbol);
            builder.AppendLine("\tTotal Quantity: " + el.TotalQuantity);
            builder.AppendLine("\tCosts: ");
            foreach (var ec in el.Costs)
            {
                builder.AppendLine("\t\t" + ec.BaseCoinSymbol + "=" + ec.Cost);
            }
            builder.AppendLine("\tTotal Value: " + el.TotalValue);

            return builder.ToString();
        }

        public string PrintElements(PortfolioContext db)
        {
            var builder = new StringBuilder();
            foreach (var el in db.Elements)
            {
                builder.AppendLine(PrintElement(el));
            }
            return builder.ToString();
        }
    }
}