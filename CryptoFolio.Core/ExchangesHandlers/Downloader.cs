﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using CryptoFolio.Core.SQLite;
using Microsoft.EntityFrameworkCore;

namespace CryptoFolio.Core.ExchangesHandlers
{
    public class Downloader
    {
        private List<IHandler> siteHandlers;

        private EventWaitHandle _commandPressed;
        private EventWaitHandle _waitDownload = new AutoResetEvent(false);

        private ManageElements manageElements;
        private ManagePortfolios managePortfolios = new ManagePortfolios();
        private ManageSites manageSites = new ManageSites();

        private bool stopped = false;

        public Downloader(EventWaitHandle commandPressed, ManageElements manageElements,
            EventWaitHandle waitDownload)
        {
            this._commandPressed = commandPressed;
            this.manageElements = manageElements;
            this._waitDownload = waitDownload;

            siteHandlers = new List<IHandler>
            {
                new BittrexHandler(),
                new LiquiHandler()
            };
        }

        public async void Download()
        {
            do
            {
                // Continue only if command isn't pressed
                _commandPressed.WaitOne();

                foreach (var siteHandler in siteHandlers)
                {
                    using (var context = new PortfolioContext())
                    {
                        await siteHandler.Update(context);

                        // Update elements
                        manageElements.UpdateElementsValue(context);

                        // Update portfolio
                        managePortfolios.UpdatePortfolio(context, 1);
                    }
                }
                
                // When it finish update, send signal to presentationT
                _waitDownload.Set();

                // Sleep for 5 seconds, so we are not abusing APIs
                Thread.Sleep(5000);
            } while (stopped == false);

            // Make sure the wait handle is set, so refresh data doesn't wait
            _waitDownload.Set();

            System.Console.WriteLine("Download thread end");
        }

        public async Task InitialLoad()
        {
            using (var db = new PortfolioContext())
            {
                manageSites.CreateSite(db, "Liqui", "https://liqui.io");
                manageSites.CreateSite(db, "Bittrex", "https://bittrex.com");
                db.SaveChanges();
            }

            foreach (var siteHandler in siteHandlers)
            {
                using (var context = new PortfolioContext())
                {
                    await siteHandler.Load(context);
                }
            }
        }

        public void Stop()
        {
            stopped = true;
        }
    }
}
