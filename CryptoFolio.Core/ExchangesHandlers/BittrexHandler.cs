﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CryptoFolio.Core.SQLite;
using CryptoFolio.Core.SQLite.Model;
using ExchangesApi;
using ExchangesApi.Exchanges.BittrexApi;

namespace CryptoFolio.Core.ExchangesHandlers
{
    public class BittrexHandler : IHandler
    {
        private Bittrex bittrexApi;

        public BittrexHandler()
        {
            this.bittrexApi = new Bittrex(new Maybe<IDownloadData>());
        }

        public string SiteName { get; } = "Bittrex";

        public async Task Load(PortfolioContext db)
        {
            var data = await bittrexApi.GetCurrencies();
            var site = db.Sites.Single(s => s.SiteName.Equals(SiteName));

            foreach (var curr in data.Result)
            {
                // Check if a coin with this symbol is already present
                if (db.Coins.Any(c => c.CoinSymbol.Equals(curr.Currency)))
                {
                    db.CoinSites.Add(new CoinSite()
                    {
                        Coin = db.Coins.Single(c => c.CoinSymbol.Equals(curr.Currency)),
                        Site = site
                    });
                }
                else
                {
                    var coin = new Coin()
                    {
                        CoinSymbol = curr.Currency,
                        CoinName = curr.CurrencyLong
                    };
                    db.Coins.Add(coin);
                    db.CoinSites.Add(new CoinSite()
                    {
                        Coin = coin,
                        Site = site
                    });
                }
            }
            db.SaveChanges();

            LoadMarkets(db).Wait();
        }

        private async Task LoadMarkets(PortfolioContext db)
        {
            var summ = await bittrexApi.GetMarketSummaries();

            foreach (var m in summ.Result)
            {
                var coins = m.MarketName.Split('-');
                var bas = coins[0];
                var exch = coins[1];

                db.Markets.Add(new Market()
                {
                    Name = m.MarketName,
                    Site = db.Sites.Single(s => s.SiteName.Equals(SiteName)),
                    NameExtended = string.Empty,
                    Last = m.Last,
                    Ask = m.Ask,
                    Bid = m.Bid,
                    VolumeAsk = 0,
                    VolumeBid = 0,
                    ExchangeCoin = db.Coins.Single(c => c.CoinSymbol.Equals(exch)),
                    BaseCoin = db.Coins.Single(c => c.CoinSymbol.Equals(bas))
                });
            }
            db.SaveChanges();
        }

        public async Task Update(PortfolioContext db)
        {
            var summ = await bittrexApi.GetMarketSummaries();

            foreach (var market in summ.Result)
            {
                var coins = market.MarketName.Split('-');
                var bas = coins[0];
                var exch = coins[1];

                // Find correct market
                var dbMarket = db.Markets.Single(m => m.BaseCoinSymbol.Equals(bas) &&
                                                      m.ExchangeCoinSymbol.Equals(exch) &&
                                                      m.SiteName.Equals(SiteName));

                // Update parameters
                dbMarket.Ask = market.Ask;
                dbMarket.Bid = market.Bid;
                dbMarket.Last = market.Last;
            }
        }
    }
}
