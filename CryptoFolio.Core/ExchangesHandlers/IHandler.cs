﻿using System.Threading.Tasks;
using CryptoFolio.Core.SQLite;

namespace CryptoFolio.Core.ExchangesHandlers
{
    interface IHandler
    {
        string SiteName { get; }

        Task Load(PortfolioContext db);
        Task Update(PortfolioContext db);
    }
}
