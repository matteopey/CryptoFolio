﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CryptoFolio.Core.SQLite;
using CryptoFolio.Core.SQLite.Model;
using ExchangesApi;
using ExchangesApi.Exchanges.LiquiApi;
using ExchangesApi.Exchanges.LiquiApi.Data;

namespace CryptoFolio.Core.ExchangesHandlers
{
    public class LiquiHandler : IHandler
    {
        private Liqui liquiApi;

        public LiquiHandler()
        {
            this.liquiApi = new Liqui(new Maybe<IDownloadData>());
        }

        public string SiteName { get; } = "Liqui";

        public async Task Load(PortfolioContext db)
        {
            var marketsInfo = await liquiApi.MarketsInfo();

            var site = db.Sites.Single(s => s.SiteName.Equals(SiteName));

            var coinsList = new List<string>();
            foreach (var c in marketsInfo.Pairs)
            {
                string[] coins = c.Key.Split('_');

                // ToUpper for universal policy
                var exc = coins[0].ToUpper();
                var bas = coins[1].ToUpper();

                if (!coinsList.Contains(exc))
                {
                    coinsList.Add(exc);
                }

                if (!coinsList.Contains(bas))
                {
                    coinsList.Add(bas);
                }
            }

            foreach (var curr in coinsList)
            {
                // Check if a coin with this symbol is already present
                if (db.Coins.Any(c => c.CoinSymbol.Equals(curr)))
                {
                    db.CoinSites.Add(new CoinSite()
                    {
                        Coin = db.Coins.Single(c => c.CoinSymbol.Equals(curr)),
                        Site = site
                    });
                }
                else
                {
                    var coin = new Coin()
                    {
                        CoinSymbol = curr,
                        CoinName = ""
                    };
                    db.Coins.Add(coin);
                    db.CoinSites.Add(new CoinSite()
                    {
                        Coin = coin,
                        Site = site
                    });
                }
            }
            db.SaveChanges();

            LoadMarkets(db, marketsInfo).Wait();
        }

        private async Task LoadMarkets(PortfolioContext db, MarketsInfo marketsInfo)
        {
            var marketsTicker = await liquiApi.MarketsTicker(marketsInfo.Pairs.Keys.ToList());

            foreach (var m in marketsTicker)
            {
                var coins = m.Key.Split('_');
                var exch = coins[0].ToUpper();
                var bas = coins[1].ToUpper();
                db.Markets.Add(new Market()
                {
                    Name = m.Key.ToUpper(),
                    Site = db.Sites.Single(s => s.SiteName.Equals(SiteName)),
                    NameExtended = string.Empty,
                    Last = m.Value.Last,
                    Ask = m.Value.Sell,
                    Bid = m.Value.Buy,
                    VolumeAsk = 0,
                    VolumeBid = 0,
                    ExchangeCoin = db.Coins.Single(c => c.CoinSymbol.Equals(exch)),
                    BaseCoin = db.Coins.Single(c => c.CoinSymbol.Equals(bas))
                });
            }
            db.SaveChanges();
        }

        public async Task Update(PortfolioContext db)
        {
            var marketsInfo = await liquiApi.MarketsInfo();
            var marketsTicker = await liquiApi.MarketsTicker(marketsInfo.Pairs.Keys.ToList());

            foreach (var market in marketsTicker)
            {
                var coins = market.Key.Split('_');
                var exch = coins[0].ToUpper();
                var bas = coins[1].ToUpper();

                // Find correct market
                var dbMarket = db.Markets.Single(m => m.BaseCoinSymbol.Equals(bas) &&
                                                      m.ExchangeCoinSymbol.Equals(exch) &&
                                                      m.SiteName.Equals(SiteName));

                // Update parameters
                dbMarket.Ask = market.Value.Sell;
                dbMarket.Bid = market.Value.Buy;
                dbMarket.Last = market.Value.Last;
            }
            db.SaveChanges();
        }
    }
}
