﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CryptoFolio.Core
{
    public class Utils
    {
        /// <summary>
        /// Converts a DateTime object into a date in UNIX format.
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public static int ToUnixTime(DateTime date)
        {
            return (int)date.Subtract(new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc))
                .TotalSeconds;
        }

        /// <summary>
        /// Convert a UNIX formatted date into a DateTime object.
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public static DateTime FromUnixTime(int date)
        {
            return new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc)
                .AddSeconds(date)
                .ToUniversalTime();
        }
    }
}
