﻿using System.Text;
using CryptoFolio.Core.SQLite;

namespace CryptoFolio.Core
{
    public class ManageCoins
    {
        public string PrintCoins(PortfolioContext db)
        {
            var builder = new StringBuilder();
            builder.AppendLine("**** COINS ****");
            foreach (var c in db.Coins)
            {
                builder.AppendLine("\tSymbol:\t" + c.CoinSymbol);
                if (!c.CoinName.Equals(""))
                    builder.AppendLine("\tName:  \t" + c.CoinName);
            }
            return builder.ToString();
        }
    }
}