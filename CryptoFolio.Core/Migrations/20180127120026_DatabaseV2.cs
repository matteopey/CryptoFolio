﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace CryptoFolio.Migrations
{
    public partial class DatabaseV2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Coins",
                columns: table => new
                {
                    CoinSymbol = table.Column<string>(type: "TEXT", nullable: false),
                    CoinName = table.Column<string>(type: "TEXT", nullable: true)
                },
                constraints: table => { table.PrimaryKey("PK_Coins", x => x.CoinSymbol); });

            migrationBuilder.CreateTable(
                name: "Sites",
                columns: table => new
                {
                    SiteName = table.Column<string>(type: "TEXT", nullable: false),
                    Url = table.Column<string>(type: "TEXT", nullable: true)
                },
                constraints: table => { table.PrimaryKey("PK_Sites", x => x.SiteName); });

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    UserName = table.Column<string>(type: "TEXT", nullable: false)
                },
                constraints: table => { table.PrimaryKey("PK_Users", x => x.UserName); });

            migrationBuilder.CreateTable(
                name: "CoinSites",
                columns: table => new
                {
                    SiteName = table.Column<string>(type: "TEXT", nullable: false),
                    CoinSymbol = table.Column<string>(type: "TEXT", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CoinSites", x => new {x.SiteName, x.CoinSymbol});
                    table.ForeignKey(
                        name: "FK_CoinSites_Coins_CoinSymbol",
                        column: x => x.CoinSymbol,
                        principalTable: "Coins",
                        principalColumn: "CoinSymbol",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CoinSites_Sites_SiteName",
                        column: x => x.SiteName,
                        principalTable: "Sites",
                        principalColumn: "SiteName",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Markets",
                columns: table => new
                {
                    ExchangeCoinSymbol = table.Column<string>(type: "TEXT", nullable: false),
                    BaseCoinSymbol = table.Column<string>(type: "TEXT", nullable: false),
                    SiteName = table.Column<string>(type: "TEXT", nullable: false),
                    Ask = table.Column<double>(type: "REAL", nullable: false),
                    Bid = table.Column<double>(type: "REAL", nullable: false),
                    Last = table.Column<double>(type: "REAL", nullable: false),
                    Name = table.Column<string>(type: "TEXT", nullable: true),
                    NameExtended = table.Column<string>(type: "TEXT", nullable: true),
                    VolumeAsk = table.Column<double>(type: "REAL", nullable: false),
                    VolumeBid = table.Column<double>(type: "REAL", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Markets", x => new {x.ExchangeCoinSymbol, x.BaseCoinSymbol, x.SiteName});
                    table.ForeignKey(
                        name: "FK_Markets_Coins_BaseCoinSymbol",
                        column: x => x.BaseCoinSymbol,
                        principalTable: "Coins",
                        principalColumn: "CoinSymbol",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Markets_Coins_ExchangeCoinSymbol",
                        column: x => x.ExchangeCoinSymbol,
                        principalTable: "Coins",
                        principalColumn: "CoinSymbol",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Markets_Sites_SiteName",
                        column: x => x.SiteName,
                        principalTable: "Sites",
                        principalColumn: "SiteName",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Portfolios",
                columns: table => new
                {
                    PortfolioId = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    CoinSymbol = table.Column<string>(type: "TEXT", nullable: true),
                    TotalValue = table.Column<double>(type: "REAL", nullable: false),
                    UserName = table.Column<string>(type: "TEXT", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Portfolios", x => x.PortfolioId);
                    table.ForeignKey(
                        name: "FK_Portfolios_Coins_CoinSymbol",
                        column: x => x.CoinSymbol,
                        principalTable: "Coins",
                        principalColumn: "CoinSymbol",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Portfolios_Users_UserName",
                        column: x => x.UserName,
                        principalTable: "Users",
                        principalColumn: "UserName",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Orders",
                columns: table => new
                {
                    OrderId = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    BaseCoinSymbol = table.Column<string>(type: "TEXT", nullable: true),
                    Cost = table.Column<double>(type: "REAL", nullable: false),
                    Date = table.Column<DateTime>(type: "TEXT", nullable: false),
                    ExchangeCoinSymbol = table.Column<string>(type: "TEXT", nullable: true),
                    OldFunds = table.Column<bool>(type: "INTEGER", nullable: false),
                    Price = table.Column<double>(type: "REAL", nullable: false),
                    Quantity = table.Column<double>(type: "REAL", nullable: false),
                    SiteName = table.Column<string>(type: "TEXT", nullable: true),
                    Type = table.Column<string>(type: "TEXT", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Orders", x => x.OrderId);
                    table.ForeignKey(
                        name: "FK_Orders_Markets_ExchangeCoinSymbol_BaseCoinSymbol_SiteName",
                        columns: x => new {x.ExchangeCoinSymbol, x.BaseCoinSymbol, x.SiteName},
                        principalTable: "Markets",
                        principalColumns: new[] {"ExchangeCoinSymbol", "BaseCoinSymbol", "SiteName"},
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Elements",
                columns: table => new
                {
                    CoinSymbol = table.Column<string>(type: "TEXT", nullable: false),
                    PortfolioId = table.Column<int>(type: "INTEGER", nullable: false),
                    TotalCost = table.Column<double>(type: "REAL", nullable: false),
                    TotalQuantity = table.Column<double>(type: "REAL", nullable: false),
                    TotalValue = table.Column<double>(type: "REAL", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Elements", x => new {x.CoinSymbol, x.PortfolioId});
                    table.ForeignKey(
                        name: "FK_Elements_Coins_CoinSymbol",
                        column: x => x.CoinSymbol,
                        principalTable: "Coins",
                        principalColumn: "CoinSymbol",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Elements_Portfolios_PortfolioId",
                        column: x => x.PortfolioId,
                        principalTable: "Portfolios",
                        principalColumn: "PortfolioId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ElementCosts",
                columns: table => new
                {
                    CoinSymbol = table.Column<string>(type: "TEXT", nullable: false),
                    PortfolioId = table.Column<int>(type: "INTEGER", nullable: false),
                    BaseCoinSymbol = table.Column<string>(type: "TEXT", nullable: false),
                    Cost = table.Column<double>(type: "REAL", nullable: false),
                    Price = table.Column<double>(type: "REAL", nullable: false),
                    Quantity = table.Column<double>(type: "REAL", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ElementCosts", x => new {x.CoinSymbol, x.PortfolioId, x.BaseCoinSymbol});
                    table.ForeignKey(
                        name: "FK_ElementCosts_Coins_BaseCoinSymbol",
                        column: x => x.BaseCoinSymbol,
                        principalTable: "Coins",
                        principalColumn: "CoinSymbol",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ElementCosts_Portfolios_PortfolioId",
                        column: x => x.PortfolioId,
                        principalTable: "Portfolios",
                        principalColumn: "PortfolioId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ElementCosts_Elements_CoinSymbol_PortfolioId",
                        columns: x => new {x.CoinSymbol, x.PortfolioId},
                        principalTable: "Elements",
                        principalColumns: new[] {"CoinSymbol", "PortfolioId"},
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ElementOrders",
                columns: table => new
                {
                    PortfolioId = table.Column<int>(type: "INTEGER", nullable: false),
                    CoinSymbol = table.Column<string>(type: "TEXT", nullable: false),
                    OrderId = table.Column<int>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ElementOrders", x => new {x.PortfolioId, x.CoinSymbol, x.OrderId});
                    table.ForeignKey(
                        name: "FK_ElementOrders_Orders_OrderId",
                        column: x => x.OrderId,
                        principalTable: "Orders",
                        principalColumn: "OrderId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ElementOrders_Elements_CoinSymbol_PortfolioId",
                        columns: x => new {x.CoinSymbol, x.PortfolioId},
                        principalTable: "Elements",
                        principalColumns: new[] {"CoinSymbol", "PortfolioId"},
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_CoinSites_CoinSymbol",
                table: "CoinSites",
                column: "CoinSymbol");

            migrationBuilder.CreateIndex(
                name: "IX_ElementCosts_BaseCoinSymbol",
                table: "ElementCosts",
                column: "BaseCoinSymbol");

            migrationBuilder.CreateIndex(
                name: "IX_ElementCosts_PortfolioId",
                table: "ElementCosts",
                column: "PortfolioId");

            migrationBuilder.CreateIndex(
                name: "IX_ElementOrders_OrderId",
                table: "ElementOrders",
                column: "OrderId");

            migrationBuilder.CreateIndex(
                name: "IX_ElementOrders_CoinSymbol_PortfolioId",
                table: "ElementOrders",
                columns: new[] {"CoinSymbol", "PortfolioId"});

            migrationBuilder.CreateIndex(
                name: "IX_Elements_PortfolioId",
                table: "Elements",
                column: "PortfolioId");

            migrationBuilder.CreateIndex(
                name: "IX_Markets_BaseCoinSymbol",
                table: "Markets",
                column: "BaseCoinSymbol");

            migrationBuilder.CreateIndex(
                name: "IX_Markets_SiteName",
                table: "Markets",
                column: "SiteName");

            migrationBuilder.CreateIndex(
                name: "IX_Orders_ExchangeCoinSymbol_BaseCoinSymbol_SiteName",
                table: "Orders",
                columns: new[] {"ExchangeCoinSymbol", "BaseCoinSymbol", "SiteName"});

            migrationBuilder.CreateIndex(
                name: "IX_Portfolios_CoinSymbol",
                table: "Portfolios",
                column: "CoinSymbol");

            migrationBuilder.CreateIndex(
                name: "IX_Portfolios_UserName",
                table: "Portfolios",
                column: "UserName");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CoinSites");

            migrationBuilder.DropTable(
                name: "ElementCosts");

            migrationBuilder.DropTable(
                name: "ElementOrders");

            migrationBuilder.DropTable(
                name: "Orders");

            migrationBuilder.DropTable(
                name: "Elements");

            migrationBuilder.DropTable(
                name: "Markets");

            migrationBuilder.DropTable(
                name: "Portfolios");

            migrationBuilder.DropTable(
                name: "Sites");

            migrationBuilder.DropTable(
                name: "Coins");

            migrationBuilder.DropTable(
                name: "Users");
        }
    }
}