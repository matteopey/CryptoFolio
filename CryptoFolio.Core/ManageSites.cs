﻿using CryptoFolio.Core.SQLite;
using CryptoFolio.Core.SQLite.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace CryptoFolio.Core
{
    public class ManageSites
    {
        public void CreateSite(PortfolioContext db, string name, string url)
        {
            db.Add(new Site
            {
                SiteName = name,
                Url = url
            });

            db.SaveChanges();
        }
    }
}
