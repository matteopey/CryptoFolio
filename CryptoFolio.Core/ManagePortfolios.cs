﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using CryptoFolio.Core.SQLite;
using CryptoFolio.Core.SQLite.Model;

namespace CryptoFolio.Core
{
    public class ManagePortfolios
    {
        public void CreatePortfolio(PortfolioContext db, Coin referenceCoin, User user)
        {
            var portfolio = new Portfolio
            {
                Coin = referenceCoin,
                TotalValue = 0,
                Elements = new List<Element>(),
                User = user
            };

            db.Portfolios.Add(portfolio);
            db.SaveChanges();
        }

        public void UpdatePortfolio(PortfolioContext db, int portfolioId)
        {
            var portfolio = db.Portfolios.Single(p => p.PortfolioId == portfolioId); // Actual Id

            portfolio.TotalValue = 0;
            foreach (var el in db.Elements)
            {
                portfolio.TotalValue += el.TotalValue;
            }
            db.SaveChanges();
        }

        public string PrintPortfolio(PortfolioContext db, int id)
        {
            var builder = new StringBuilder();
            var portfolio = db.Portfolios.Single(p => p.PortfolioId == id);

            builder.AppendLine("**** PORTFOLIO ****");
            builder.AppendLine("\tTotalValue: " + portfolio.TotalValue);

            return builder.ToString();
        }
    }
}