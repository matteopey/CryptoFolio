﻿namespace CryptoFolio.Core
{
    public class Candle
    {
        public int CandleUnit { get; set; }
        public int Multiplier { get; set; }

        public Candle(int unit, int multiplier)
        {
            this.CandleUnit = unit;
            this.Multiplier = multiplier;
        }
    }
}
