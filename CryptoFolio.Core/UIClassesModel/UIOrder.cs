﻿using System;

namespace CryptoFolio.Core.UIClassesModel
{
    public class UIOrder
    {
        public string ExchangeCoin { get; set; }
        public string BaseCoin { get; set; }
        public string SiteName { get; set; }
        public string Type { get; set; }
        public bool OldFunds { get; set; }
        public double Quantity { get; set; }
        public double Price { get; set; }
        public double Cost { get; set; }
        public DateTime Date { get; set; }
    }
}