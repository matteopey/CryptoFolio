﻿using System.Collections.Generic;
using CryptoFolio.Core.SQLite;
using CryptoFolio.Core.SQLite.Model;

namespace CryptoFolio.Core
{
    public class ManageUsers
    {
        public User CreateUser(PortfolioContext db, string userName)
        {
            var user = new User
            {
                Portfolios = new List<Portfolio>(),
                UserName = userName
            };

            db.Users.Add(user);
            return user;
        }
    }
}