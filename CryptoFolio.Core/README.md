# CryptoFolio.Core

Code for the Core components of the software.
All the database logic is here.

### Build
```shell
dotnet restore
dotnet build -c Debug
```