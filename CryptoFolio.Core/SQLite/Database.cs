﻿using CryptoFolio.Core.SQLite.Model;
using Microsoft.EntityFrameworkCore;

namespace CryptoFolio.Core.SQLite
{
    public class PortfolioContext : DbContext
    {
        public DbSet<Coin> Coins { get; set; }
        public DbSet<Element> Elements { get; set; }
        public DbSet<Market> Markets { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<Portfolio> Portfolios { get; set; }
        public DbSet<Site> Sites { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<ElementCost> ElementCosts { get; set; }
        public DbSet<CoinSite> CoinSites { get; set; }
        public DbSet<ElementOrder> ElementOrders { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // User key
            modelBuilder.Entity<User>()
                .HasKey(u => u.UserName);

            // one-to-many User-Portfolio
            modelBuilder.Entity<Portfolio>()
                .HasOne(p => p.User)
                .WithMany(u => u.Portfolios);

            // Element keys
            modelBuilder.Entity<Element>()
                .HasKey(e => new {e.CoinSymbol, e.PortfolioId});
            // Element relationships
            modelBuilder.Entity<Element>()
                .HasOne(e => e.Portfolio)
                .WithMany(p => p.Elements);
            modelBuilder.Entity<Element>()
                .HasOne(e => e.Coin)
                .WithMany(c => c.Elements);

            // Element-Cost
            modelBuilder.Entity<ElementCost>()
                .HasKey(ec => new {ec.CoinSymbol, ec.PortfolioId, ec.BaseCoinSymbol});
            modelBuilder.Entity<ElementCost>()
                .HasOne(ec => ec.Element)
                .WithMany(e => e.Costs);
            modelBuilder.Entity<ElementCost>()
                .HasOne(ec => ec.BaseCoin)
                .WithMany(c => c.Costs)
                .HasForeignKey(ec => ec.BaseCoinSymbol);

            // Coin key
            modelBuilder.Entity<Coin>()
                .HasKey(c => c.CoinSymbol);
            modelBuilder.Entity<Coin>()
                .Ignore(e => e.Markets);

            // Site key
            modelBuilder.Entity<Site>()
                .HasKey(s => s.SiteName);

            // Coin-Site many-to-many
            modelBuilder.Entity<CoinSite>()
                .HasKey(s => new {s.SiteName, s.CoinSymbol});
            modelBuilder.Entity<CoinSite>()
                .HasOne(cs => cs.Site)
                .WithMany(s => s.CoinSites)
                .HasForeignKey(cs => cs.SiteName);
            modelBuilder.Entity<CoinSite>()
                .HasOne(cs => cs.Coin)
                .WithMany(c => c.CoinSites)
                .HasForeignKey(cs => cs.CoinSymbol);

            // Order-Market relationship
            modelBuilder.Entity<Order>()
                .HasOne(o => o.Market)
                .WithMany(m => m.Orders)
                .HasForeignKey(o => new {o.ExchangeCoinSymbol, o.BaseCoinSymbol, o.SiteName});

            // Element-Order n-n
            modelBuilder.Entity<ElementOrder>()
                .HasKey(s => new {s.PortfolioId, s.CoinSymbol, s.OrderId});
            modelBuilder.Entity<ElementOrder>()
                .HasOne(cs => cs.Element)
                .WithMany(s => s.ElementOrders)
                .HasForeignKey(cs => new {cs.CoinSymbol, cs.PortfolioId});
            modelBuilder.Entity<ElementOrder>()
                .HasOne(cs => cs.Order)
                .WithMany(c => c.ElementOrders)
                .HasForeignKey(cs => cs.OrderId);

            // Market key
            modelBuilder.Entity<Market>()
                .HasKey(m => new {m.ExchangeCoinSymbol, m.BaseCoinSymbol, m.SiteName});
            // Market-Site relationship
            modelBuilder.Entity<Market>()
                .HasOne(m => m.Site)
                .WithMany(s => s.Markets)
                .HasForeignKey(m => m.SiteName);
            modelBuilder.Entity<Market>()
                .HasOne(m => m.BaseCoin)
                .WithMany("Market1")
                .HasForeignKey(m => m.BaseCoinSymbol);
            modelBuilder.Entity<Market>()
                .HasOne(m => m.ExchangeCoin)
                .WithMany("Market2")
                .HasForeignKey(m => m.ExchangeCoinSymbol);
            
            // Ignore classes for historical trend
            modelBuilder.Ignore<Tick>();
            modelBuilder.Ignore<Candle>();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite("Data Source=database.db");
        }
    }
}