﻿namespace CryptoFolio.Core.SQLite.Model
{
    public class ElementOrder
    {
        public string CoinSymbol { get; set; }
        public int PortfolioId { get; set; }
        public Element Element { get; set; }

        public int OrderId { get; set; }
        public Order Order { get; set; }
    }
}