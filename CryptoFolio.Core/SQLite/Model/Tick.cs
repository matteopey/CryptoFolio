using System;
using System.Collections;

namespace CryptoFolio.Core.SQLite.Model
{
    // Represent a tick for the element and portfolio trendlines
    public class Tick
    {
        // Point in time expressed in UNIX timestamp
        public DateTime Time { get; set; }
        
        public double Value { get; set; }
    }
}