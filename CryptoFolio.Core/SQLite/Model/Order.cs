﻿using System;
using System.Collections.Generic;

namespace CryptoFolio.Core.SQLite.Model
{
    public class Order
    {
        // Primary key
        public int OrderId { get; set; }

        public double Quantity { get; set; }
        public double Cost { get; set; }
        public double Price { get; set; }
        public DateTime Date { get; set; }

        public bool OldFunds { get; set; }

        public string Type { get; set; }

        // Market foreign key
        public string ExchangeCoinSymbol { get; set; }

        public string BaseCoinSymbol { get; set; }
        public string SiteName { get; set; }
        public Market Market { get; set; }

        // Join table element-order
        public IList<ElementOrder> ElementOrders { get; set; }
    }
}