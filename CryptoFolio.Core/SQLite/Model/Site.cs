﻿using System.Collections.Generic;

namespace CryptoFolio.Core.SQLite.Model
{
    public class Site
    {
        public string SiteName { get; set; }
        public string Url { get; set; }

        // Join table coin-site
        public IList<CoinSite> CoinSites { get; set; }

        // List of markets
        public IList<Market> Markets { get; set; }
    }
}