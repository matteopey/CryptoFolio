﻿using System.Collections.Generic;
using System.Linq;

namespace CryptoFolio.Core.SQLite.Model
{
    public class Coin
    {
        // Key
        public string CoinSymbol { get; set; }

        public string CoinName { get; set; }

        // List of portfolios where the coin is reference
        public IList<Portfolio> Portfolios { get; set; }

        // List of elements where the coin is key
        public IList<Element> Elements { get; set; }

        // Join table coin-site
        public IList<CoinSite> CoinSites { get; set; }

        // List of markets where the coin is present
        public IEnumerable<Market> Markets
            => Market1.Concat(Market2).Distinct();

        private ICollection<Market> Market1 { get; set; }
        private ICollection<Market> Market2 { get; set; }

        // Costs where the Coin is present
        public IList<ElementCost> Costs { get; set; }
    }
}