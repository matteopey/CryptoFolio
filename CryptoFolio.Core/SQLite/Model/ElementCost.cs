﻿namespace CryptoFolio.Core.SQLite.Model
{
    public class ElementCost
    {
        // Key
        public string CoinSymbol { get; set; }

        public int PortfolioId { get; set; }
        public Portfolio Portfolio { get; set; }
        public Coin BaseCoin { get; set; }
        public string BaseCoinSymbol { get; set; }

        public double Cost { get; set; }
        public double Price { get; set; }
        public double Quantity { get; set; }

        public Element Element { get; set; }
    }
}