﻿using System.Collections.Generic;

namespace CryptoFolio.Core.SQLite.Model
{
    public class Element
    {
        // Two keys
        public int PortfolioId { get; set; }

        public string CoinSymbol { get; set; }

        // Total value of element based on ReferenceCoin
        public double TotalValue { get; set; }

        // Total quantity of coins
        public double TotalQuantity { get; set; }

        // Total cost in reference coin
        public double TotalCost { get; set; }

        // Portfolio
        public Portfolio Portfolio { get; set; }

        // Coin of the element
        public Coin Coin { get; set; }

        // List of pair for tracking cost
        public IList<ElementCost> Costs { get; set; }

        // Join table element-order
        public IList<ElementOrder> ElementOrders { get; set; }

        // Trend over time
        public IList<Tick> Trend { get; set; }

        // Candle for historical trend
        public Candle Candle { get; set; }
    }
}