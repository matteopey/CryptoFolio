﻿using System.Collections.Generic;

namespace CryptoFolio.Core.SQLite.Model
{
    public class Market
    {
        // Three primary keys
        public string ExchangeCoinSymbol { get; set; }

        public Coin ExchangeCoin { get; set; }
        public string BaseCoinSymbol { get; set; }
        public Coin BaseCoin { get; set; }
        public string SiteName { get; set; }

        public string Name { get; set; }
        public string NameExtended { get; set; }

        public double Last { get; set; }
        public double Ask { get; set; }
        public double Bid { get; set; }
        public double VolumeAsk { get; set; }
        public double VolumeBid { get; set; }

        // List of orders made on the market
        public IList<Order> Orders { get; set; }

        // Site object
        public Site Site { get; set; }
    }
}