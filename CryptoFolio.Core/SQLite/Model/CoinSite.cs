﻿namespace CryptoFolio.Core.SQLite.Model
{
    public class CoinSite
    {
        public string SiteName { get; set; }
        public Site Site { get; set; }

        public string CoinSymbol { get; set; }
        public Coin Coin { get; set; }
    }
}