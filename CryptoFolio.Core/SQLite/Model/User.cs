﻿using System.Collections.Generic;

namespace CryptoFolio.Core.SQLite.Model
{
    public class User
    {
        // User Name        
        public string UserName { get; set; }

        // Portfolios
        public IList<Portfolio> Portfolios { get; set; }
    }
}