﻿using System.Collections.Generic;

namespace CryptoFolio.Core.SQLite.Model
{
    public class Portfolio
    {
        // Portfolio Id        
        public int PortfolioId { get; set; }

        // Total value of all portfolio in reference coin
        public double TotalValue { get; set; }

        // User
        public string UserName { get; set; }

        public User User { get; set; }

        // Reference coin
        public string CoinSymbol { get; set; }

        public Coin Coin { get; set; }

        // Elements
        public IList<Element> Elements { get; set; }
    }
}