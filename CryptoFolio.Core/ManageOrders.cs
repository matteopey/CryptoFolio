﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks.Dataflow;
using CryptoFolio.Core.Exceptions;
using CryptoFolio.Core.SQLite;
using CryptoFolio.Core.SQLite.Model;
using CryptoFolio.Core.UIClassesModel;
using Microsoft.EntityFrameworkCore;

namespace CryptoFolio.Core
{
    public class ManageOrders
    {
        private ManageElements manageElements;
        private BufferBlock<string> log;

        public ManageOrders(BufferBlock<string> logger)
        {
            log = logger;
            manageElements = new ManageElements(log);
        }

        public Order CreateOrder(PortfolioContext db, UIOrder order)
        {
            var market = db.Markets.Single(m => m.BaseCoinSymbol.Equals(order.BaseCoin) &&
                                                m.ExchangeCoinSymbol.Equals(order.ExchangeCoin) &&
                                                m.SiteName.Equals(order.SiteName));

            // Create both elements if it's needed
            Element exchEl;

            var exchangeCoin = db.Coins.Single(c => c.CoinSymbol.Equals(order.ExchangeCoin));
            var baseCoin = db.Coins.Single(c => c.CoinSymbol.Equals(order.BaseCoin));            

            try
            {
                exchEl = db.Elements.Single(e => e.Coin == exchangeCoin);
            }
            catch (InvalidOperationException)
            {
                log.SendAsync("Not found exchange element => creating new one.");
                exchEl = manageElements.CreateElement(db, exchangeCoin);
            }

            Element baseEl;
            try
            {
                baseEl = db.Elements.Single(e => e.Coin == baseCoin);
            }
            catch (InvalidOperationException)
            {
                log.SendAsync("Not found exchange element => creating new one.");
                baseEl = manageElements.CreateElement(db, baseCoin);
            }

            // Create order
            var dbOrder = new Order
            {
                Market = market,
                Date = order.Date,
                Quantity = order.Quantity,
                Price = order.Price,
                Cost = order.Cost,
                Type = order.Type,
                OldFunds = order.OldFunds,
            };

            // Add to ElementOrder
            var exchElementOrder = new ElementOrder
            {
                Element = exchEl,
                Order = dbOrder
            };
            var baseElementOrder = new ElementOrder
            {
                Element = baseEl,
                Order = dbOrder
            };

            db.ElementOrders.Add(exchElementOrder);
            db.ElementOrders.Add(baseElementOrder);
            db.Orders.Add(dbOrder);
            db.SaveChanges();
            return dbOrder;
        }

        public void ModifyOrder(PortfolioContext db, Order oldOrder, ManageElements manageElements,
            ManagePortfolios managePortfolios, string modifier, IList<object> param)
        {
            // Copy order for reading later            
            var newOrder = new Order
            {
                Cost = oldOrder.Cost,
                Date = oldOrder.Date,
                Market = oldOrder.Market,
                OldFunds = oldOrder.OldFunds,
                OrderId = oldOrder.OrderId,
                Price = oldOrder.Price,
                Quantity = oldOrder.Quantity,
                Type = oldOrder.Type
            };

            switch (modifier)
            {
                case "Quantity":
                    var quantity = (double) param[0];

                    if (oldOrder.Type.Equals("Buy"))
                    {
                        //TODO: prendi solo gli ordini successivi a questo della modifica
                        // Verify (scenario_1_1)
                        // Take all Sell ETH order
                        var sumQ = db.Orders.Where(o =>
                                o.Market.ExchangeCoinSymbol.Equals(oldOrder.Market.ExchangeCoinSymbol) &&
                                o.Type.Equals("Sell"))
                            .Sum(o => o.Quantity);
                        if (sumQ > quantity)
                        {
                            string message =
                                $"Your SELL orders needs at least {sumQ} {oldOrder.Market.ExchangeCoinSymbol}";
                            throw new Exception(message);
                        }
                    }
                    else
                    {
                        // Verify (scenario_1_3)
                        // Take all Sell ETH order
                        var sumQ = db.Orders.Where(o =>
                                o.Market.ExchangeCoinSymbol.Equals(oldOrder.Market.ExchangeCoinSymbol) &&
                                o.Type.Equals("Buy"))
                            .Sum(o => o.Quantity);
                        if (sumQ < quantity)
                        {
                            string message =
                                $"You bought {sumQ} {oldOrder.Market.ExchangeCoinSymbol}, not enough to sell {quantity}";
                            throw new Exception(message);
                        }
                    }

                    newOrder.Quantity = quantity;
                    newOrder.Cost = newOrder.Quantity * newOrder.Price;

                    // Update the order on the db
                    oldOrder.Quantity = quantity;
                    oldOrder.Cost = oldOrder.Quantity * oldOrder.Price;

                    //UpdateElementsOnModifyOrder(db, oldOrder, newOrder, "Quantity");
                    manageElements.UpdateElementsOnModifyOrderV2(db, newOrder);
                    break;
                case "Price":
                    // TODO: verify cost
                    var price = (double) param[0];

                    newOrder.Price = price;
                    newOrder.Cost = newOrder.Quantity * newOrder.Price;

                    // Update the order on the db
                    oldOrder.Price = price;
                    oldOrder.Cost = oldOrder.Quantity * oldOrder.Price;

                    //UpdateElementsOnModifyOrder(db, oldOrder, newOrder, "Price");
                    manageElements.UpdateElementsOnModifyOrderV2(db, newOrder);
                    break;

                case "Both":
                    // TODO: verifiche
                    var q = (double) param[0];
                    var p = (double) param[1];

                    newOrder.Quantity = q;
                    newOrder.Price = p;
                    newOrder.Cost = newOrder.Quantity * newOrder.Price;

                    oldOrder.Quantity = q;
                    oldOrder.Price = p;
                    oldOrder.Cost = oldOrder.Quantity * oldOrder.Price;

                    manageElements.UpdateElementsOnModifyOrderV2(db, newOrder);
                    break;
                case "Type":
                    // TODO: verifiche
                    if (oldOrder.Type.Equals("Buy"))
                    {
                        newOrder.Type = "Sell";
                        oldOrder.Type = "Sell";
                    }
                    else
                    {
                        newOrder.Type = "Buy";
                        oldOrder.Type = "Buy";
                    }
                    manageElements.UpdateElementsOnModifyOrderV2(db, newOrder);
                    break;
                case "Oldfunds":
                    //TODO: verifiche
                    if (oldOrder.OldFunds)
                    {
                        oldOrder.OldFunds = false;
                        newOrder.OldFunds = false;
                    }
                    else
                    {
                        oldOrder.OldFunds = true;
                        newOrder.OldFunds = true;
                    }
                    manageElements.UpdateElementsOnModifyOrderV2(db, newOrder);
                    break;
            }

            db.SaveChanges();
        }

        public void DeleteOrder(PortfolioContext db, Order order)
        {
            //NOTE: Verifica 1.a (OneNote)
            // Sum of buy >= sum of sell, can't sell something you don't have
            if (order.Type.Equals("Buy"))
            {
                var sellOrders = db.Orders.Where(o => o.Market.ExchangeCoin == order.Market.ExchangeCoin &&
                                                      o.Type.Equals("Sell") &&
                                                      o.Date > order.Date)
                    .Sum(o => o.Quantity);
                var buyOrders = db.Orders.Where(o => o.Market.ExchangeCoin == order.Market.ExchangeCoin &&
                                                     o.Type.Equals("Buy") &&
                                                     o.Date > order.Date)
                    .Sum(o => o.Quantity);

                if (sellOrders > buyOrders)
                {
                    throw new OrderException(
                        $"After this order you buy {buyOrders} {order.Market.ExchangeCoin.CoinSymbol} but then you sell {sellOrders}.");
                }
            }

            // Remove from db and save
            db.Orders.Remove(order);
            db.SaveChanges();

            manageElements.UpdateElementsOnModifyOrderV2(db, order);

            //manageElements.UpdateElementsV2(db, db.Elements.Single(e => e.Coin == order.Market.BaseCoin), order.Market.Site);
            //manageElements.UpdateElementsV2(db, db.Elements.Single(e => e.Coin == order.Market.ExchangeCoin), order.Market.Site);

            db.SaveChanges();
        }

        public string PrintOrderById(PortfolioContext db, int id)
        {
            var order = db.Orders.Single(o => o.OrderId == id);
            return PrintOrder(db, order);
        }

        public string PrintOrder(PortfolioContext db, Order order)
        {
            var builder = new StringBuilder();

            builder.AppendLine("**** ORDER ****");
            builder.AppendLine("\tOrder id: " + order.OrderId);
            builder.AppendLine("\tMarket: " + order.Market.Name);
            builder.AppendLine("\tType: " + order.Type);
            builder.AppendLine("\tQuantity: " + order.Quantity);
            builder.AppendLine("\tPrice: " + order.Price);
            builder.AppendLine("\tCost: " + order.Cost);
            builder.AppendLine("\tUseOldFunds: " + order.OldFunds);
            builder.AppendLine("\tDate: " + order.Date);

            return builder.ToString();
        }

        public string PrintAllOrders(PortfolioContext db)
        {
            var builder = new StringBuilder();

            var orders = db.Orders.Include(o => o.Market).ToList();

            foreach (var order in orders)
            {
                builder.Append(PrintOrder(db, order));
            }

            return builder.ToString();
        }
    }
}