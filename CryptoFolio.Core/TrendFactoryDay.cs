﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CryptoFolio.Core.SQLite;
using CryptoFolio.Core.SQLite.Model;
using ExchangesApi;
using ExchangesApi.Exchanges.CryptoCompareApi;
using ExchangesApi.Exchanges.CryptoCompareApi.ApiCalls;

namespace CryptoFolio.Core
{
    public class TrendFactoryDay : TrendFactory
    {
        private ManageElements manageElements;

        public TrendFactoryDay(ManageElements manageElements)
        {
            this.manageElements = manageElements;
        }

        public override async Task GenerateTrend(PortfolioContext db, Element el)
        {
            //TODO: settings for default candle dimension
            //TODO: let user select candle dimension

            // Reset trend for element
            el.Trend = new List<Tick>();

            // Calculate the time we need
            // Retrieve oldest buy order
            // TODO: parti solo da un vecchio ordine in base all'operazione. Se aggiungi un ordine, parti solo dalla data di quell'ordine.
            var oldestOrder = db.ElementOrders.Where(eo => eo.CoinSymbol.Equals(el.CoinSymbol))
                .OrderBy(o => o.Order.Date)
                .First()
                .Order;

            var numberOfHours =
                (Utils.ToUnixTime(DateTime.UtcNow) - Utils.ToUnixTime(oldestOrder.Date)) /
                (el.Candle.CandleUnit * el.Candle.Multiplier);

            // Retrieve all orders for the element
            var allOrders = db.ElementOrders.Where(eo => eo.CoinSymbol.Equals(el.CoinSymbol))
                .Select(eo => eo.Order)
                .OrderBy(o => o.Date)
                .ToList();

            var quantityTrend = BuildQuantityTrend(allOrders, el);

            // Initialize current time with the date of the first (oldest) order.
            var currentTime = Utils.ToUnixTime(oldestOrder.Date);

            // Retrieve data from API online
            var (m1, m2) = manageElements.FindMarket(db, el);

            if (!m1.Any() && !m2.Any())
            {
                // The element considered is also the reference coin
                // The trend is the quantity, we can build the trend only considering the quantity over time

                // For each quantity trend built
                for (var i = 1; i < quantityTrend.Count; i++)
                {
                    var t = quantityTrend[i];
                    var tickUnixTime = Utils.ToUnixTime(t.Time);

                    // While the current time is less or equal than the time of the tick,
                    // the quantity don't change. So we can add the tick to the actual Trend.
                    while (currentTime <= tickUnixTime)
                    {
                        el.Trend.Add(new Tick
                        {
                            Time = Utils.FromUnixTime(currentTime),
                            Value = quantityTrend[i - 1].Value
                        });

                        currentTime = currentTime + el.Candle.CandleUnit * el.Candle.Multiplier;
                    }
                }

                // Add the ticks until now, with the last quantity in the quantityTrend.
                var now = Utils.ToUnixTime(DateTime.UtcNow);
                var lastQuantity = quantityTrend.Last().Value;

                while (currentTime <= now)
                {
                    el.Trend.Add(new Tick
                    {
                        Time = Utils.FromUnixTime(currentTime),
                        Value = lastQuantity
                    });

                    currentTime = currentTime + el.Candle.CandleUnit * el.Candle.Multiplier;
                }

                db.SaveChanges();
                return;
            }

            var ccApi = new CryptoCompare(new Maybe<IDownloadData>());

            // Compute numbers of api calls to make
            int dataPointsNumber; // Number of data points per API call
            double points;
            var cryptoCompareMax = 2000 / el.Candle.Multiplier;

            if (numberOfHours < cryptoCompareMax)
            {
                dataPointsNumber = numberOfHours;
                points = 1;
            }
            else
            {
                dataPointsNumber = cryptoCompareMax;
                points = Math.Ceiling((double) numberOfHours / cryptoCompareMax);
            }

            if (m1.Any() && !m2.Any())
            {
                var i = 1;
                for (var n = 1; n <= points; n++)
                {
                    // The last time for the api call, is the current time plus 2000 times
                    // the unit in seconds. For now the unit is hour, that is 3600 seconds.
                    var lastTimeUnix = currentTime +
                                       dataPointsNumber * el.Candle.CandleUnit *
                                       el.Candle.Multiplier;

                    // If we go in the future
                    if (lastTimeUnix > Utils.ToUnixTime(DateTime.UtcNow))
                    {
                        var now = Utils.ToUnixTime(DateTime.UtcNow);
                        lastTimeUnix = now;

                        // Number of hours between currentTime and now
                        dataPointsNumber = (now - currentTime) / el.Candle.CandleUnit /
                                           el.Candle.Multiplier;
                    }

                    // Exists one simple market COIN-REFERENCE (eg: ETH-BTC with BTC reference)
                    var res = await ccApi.HistoDay(m1.Single().ExchangeCoinSymbol,
                        m1.Single().BaseCoinSymbol, new Maybe<string>(),
                        new Maybe<int>(el.Candle.Multiplier),
                        new Maybe<int>(dataPointsNumber), new Maybe<int>(lastTimeUnix),
                        new Maybe<string>());

                    var lastQuantity = quantityTrend.Last().Value;

                    foreach (var ticker in res.Data)
                    {
                        // If i is out of bound, we exit this foreach, update the currentTime to
                        // the latest candle and move to create trend for the remaining tickers
                        if (i == quantityTrend.Count)
                        {
                            el.Trend.Add(new Tick
                            {
                                Time = Utils.FromUnixTime(ticker.Time),
                                Value = lastQuantity * ticker.High
                            });
                            continue;
                        }

                        // If the ticker time is greater than quantity, we need to update 
                        // the pointer to the quantity
                        if (ticker.Time > Utils.ToUnixTime(quantityTrend[i].Time))
                            i++;

                        el.Trend.Add(new Tick
                        {
                            Time = Utils.FromUnixTime(ticker.Time),
                            Value = quantityTrend[i - 1].Value * ticker.High
                        });
                    }

                    // Move to next candle
                    currentTime = res.Data.Last().Time +
                                  el.Candle.CandleUnit * el.Candle.Multiplier;
                }

                db.SaveChanges();
            }
            else
            {
                // We have the special case where the reference coin is not directly exchanged 
                // with the element.

                var i = 1;
                for (var n = 1; n <= points; n++)
                {
                    // The last time for the api call, is the current time plus 2000 times
                    // the unit in seconds. For now the unit is hour, that is 3600 seconds.
                    var lastTimeUnix = currentTime +
                                       dataPointsNumber * el.Candle.CandleUnit *
                                       el.Candle.Multiplier;

                    // If we go in the future
                    if (lastTimeUnix > Utils.ToUnixTime(DateTime.UtcNow))
                    {
                        var now = Utils.ToUnixTime(DateTime.UtcNow);
                        lastTimeUnix = now;

                        // Number of hours between currentTime and now
                        dataPointsNumber = (now - currentTime) / el.Candle.CandleUnit /
                                           el.Candle.Multiplier;
                    }

                    // Take data for the two markets.
                    // We assume that every coin is exchanged with BTC.
                    // 1. The COIN-BTC market
                    // 2. The REFERENCECOIN-BTC market.

                    var elementMarket = await ccApi.HistoDay(m1.Single().ExchangeCoinSymbol,
                        m1.Single().BaseCoinSymbol, new Maybe<string>(),
                        new Maybe<int>(el.Candle.Multiplier),
                        new Maybe<int>(dataPointsNumber), new Maybe<int>(lastTimeUnix),
                        new Maybe<string>());

                    var refCoinMarket = await ccApi.HistoDay(m2.Single().ExchangeCoinSymbol,
                        m2.Single().BaseCoinSymbol, new Maybe<string>(),
                        new Maybe<int>(el.Candle.Multiplier),
                        new Maybe<int>(dataPointsNumber), new Maybe<int>(lastTimeUnix),
                        new Maybe<string>());

                    var zippedCollection = elementMarket.Data
                        .Zip(refCoinMarket.Data, (first, second) =>
                            new List<Ticker> {first, second});

                    var lastQuantity = quantityTrend.Last().Value;

                    foreach (var ticker in zippedCollection)
                    {
                        // If i is out of bound, we exit this foreach, update the currentTime to
                        // the latest candle and move to create trend for the remaining tickers
                        if (i == quantityTrend.Count)
                        {
                            el.Trend.Add(new Tick
                            {
                                Time = Utils.FromUnixTime(ticker[0].Time),
                                Value = lastQuantity * (ticker[0].High / ticker[1].High)
                            });
                            continue;
                        }

                        // If the ticker time is greater than quantity, we need to update 
                        // the pointer to the quantity
                        if (ticker[0].Time > Utils.ToUnixTime(quantityTrend[i].Time))
                            i++;

                        el.Trend.Add(new Tick
                        {
                            Time = Utils.FromUnixTime(ticker[0].Time),
                            Value = quantityTrend[i - 1].Value * (ticker[0].High / ticker[1].High)
                        });
                    }

                    // Move to next candle
                    currentTime = elementMarket.Data.Last().Time +
                                  el.Candle.CandleUnit * el.Candle.Multiplier;
                }

                db.SaveChanges();
            }
        }
    }
}